//
//  BalanceSheetTabDataSource+XCTest.m
//  BalanceSheet
//
//  Created by Ye Ma on 2016-07-28.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "BalanceSheetTabDataSource+XCTest.h"
#import <objc/runtime.h>

static char const * const Key = "okay";

@implementation BalanceSheetTabDataSource (XCTest)

@dynamic tag;

- (void)updateDataScreen
{
    [self setTag:@(BalanceSheetViewControllerType_Report)];
}

#pragma mark - getter/setter

- (void)setTag:(id)tag
{
    objc_setAssociatedObject(self, Key, tag, OBJC_ASSOCIATION_ASSIGN);
}

- (id)tag
{
    return objc_getAssociatedObject(self, Key);
}



@end
