//
//  BalanceSheetTabDataSource+XCTest.h
//  BalanceSheet
//
//  Created by Ye Ma on 2016-07-28.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "BalanceSheetTabDataSource.h"

@protocol BalanceSheetTabDataSourceXCTestDelegate <NSObject>

- (void)updateDataScreen;

@end

@interface BalanceSheetTabDataSource (XCTest)<BalanceSheetTabDataSourceXCTestDelegate>

@property (nonatomic, assign) id tag;

@end
