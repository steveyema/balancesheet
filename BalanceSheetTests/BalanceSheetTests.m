//
//  BalanceSheetTests.m
//  BalanceSheetTests
//
//  Created by Ye Ma on 2016-05-31.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <CoreData/CoreData.h>
#import "BalanceSheetApplication.h"
#import "BalanceSheetTabDataSource.h"
#import "BalanceSheetTabDataSource+XCTest.h"

@interface BalanceSheetTests : XCTestCase
{
    XCTestExpectation *serverRespondExpectation;
}

@property(nonatomic, strong) BalanceSheetTabDataSource *incomeTabDataSource;
@property(nonatomic, strong) BalanceSheetTabDataSource *expenseTabDataSource;
@property(nonatomic, strong) BalanceSheetTabDataSource *reportTabDataSource;

@end

@implementation BalanceSheetTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    [BalanceSheetApplication sharedInstance].activeBalanceSheetTab = BalanceSheetTab_Income;
    self.incomeTabDataSource = [BalanceSheetTabDataSource new];
    
    [BalanceSheetApplication sharedInstance].activeBalanceSheetTab = BalanceSheetTab_Outcome;
    self.expenseTabDataSource = [BalanceSheetTabDataSource new];

    [BalanceSheetApplication sharedInstance].activeBalanceSheetTab = BalanceSheetTab_Report;
    self.reportTabDataSource = [BalanceSheetTabDataSource new];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testbackContextDidInsertedOnReportTab
{
//    serverRespondExpectation = [self expectationWithDescription:@"server responded"];
//
//    [self waitForExpectationsWithTimeout:10 handler:^(NSError *error) {
//        if (error) {
//            NSLog(@"Server Timeout Error: %@", error);
//        }
//        NSLog(@"execute here after delegate called  or timeout");
//    }];
    [[NSNotificationCenter defaultCenter] postNotificationName:NSManagedObjectContextDidSaveNotification object:[BalanceSheetDBController sharedInstance].backgroundManagedObjectContext userInfo:@{@"inserted" : @[@(1)]}];

    XCTAssertTrue([self.reportTabDataSource.tag isEqual:@(BalanceSheetViewControllerType_Report)]);
}

@end
