//
//  FileHelper.m
//  BalanceSheet
//
//  Created by Ye Ma on 2016-05-31.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "FileHelper.h"

NSString *pathInAppDirectory(NSSearchPathDirectory directory, NSString *fileName, BOOL createDirectoryIfMissing);

NSString *pathInAppDirectory(NSSearchPathDirectory directory, NSString *fileName, BOOL createDirectoryIfMissing)
{
    // Get list of directories in the sandbox.
    NSArray *directories = NSSearchPathForDirectoriesInDomains(directory, NSUserDomainMask, YES);
    
    // Get one and only directory from that list
    NSString *appDirectory = [directories objectAtIndex:0];
    
    NSString *appDirectoryPath = [appDirectory stringByAppendingPathComponent:fileName];
    
    // Should we be checking whether or not the directory needs to be created?
    if (createDirectoryIfMissing == YES)
    {
        // Does the requested application directory not already exist?
        NSFileManager *fileManager = [NSFileManager defaultManager];
        BOOL dir = YES;
        if (![fileManager fileExistsAtPath:appDirectory isDirectory:&dir])
        {
            // Create the archive download directory.
            [fileManager createDirectoryAtPath:appDirectory withIntermediateDirectories:YES attributes:nil error:nil];
        }
    }
    
    // Append passed in file name to that directory, return it
    return appDirectoryPath;
}
