//
//  ReportEntry.m
//  BalanceSheet
//
//  Created by Ye Ma on 2016-06-02.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "ReportEntry.h"

@implementation ReportEntry

+(id)entryWithStartDate:(NSDate *)startDate andEndDate:(NSDate *)endDate
{
    ReportEntry *reportEntry = [[ReportEntry alloc] init];
    reportEntry.startDate = startDate;
    reportEntry.endDate = endDate;
    return reportEntry;
}

- (NSArray *)sortedDetailInstances
{
    if (!_sortedDetailInstances)
    {
        _sortedDetailInstances = [self.detailInstances sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
            MoneyEntry *entry1 = (MoneyEntry *)obj1;
            MoneyEntry *entry2 = (MoneyEntry *)obj2;
            return [entry2.datetime isOlderThanDate:entry1.datetime];
        }];
    }
    return _sortedDetailInstances;
}

@end
