//
//  ReportOperations.m
//  BalanceSheet
//
//  Created by Ye Ma on 2016-06-10.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "ReportOperations.h"

@implementation ReportOperations

- (id)init
{
    if (self = [super init])
    {
        _queue = [[NSOperationQueue alloc] init];
        _queue.maxConcurrentOperationCount = 1;
        _fetchingInProgress = [[NSMutableDictionary alloc] init];
    }
    return self;
}

@end
