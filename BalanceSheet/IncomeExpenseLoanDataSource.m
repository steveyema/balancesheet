//
//  IncomeExpenseLoanDataSource.m
//  BalanceSheet
//
//  Created by Ye Ma on 2016-07-15.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "IncomeExpenseLoanDataSource.h"
#import "ReportSectionHeaderView.h"

@interface IncomeExpenseLoanDataSource()<UITabBarDelegate, UITableViewDataSource>

@property(nonatomic, strong) NSMutableDictionary *dicOfEntries;
@property(nonatomic, strong) NSArray *sortedKeyForEntries;
@property(nonatomic, strong) MoneyEntry *selectedEntry;

- (void)fillAmountLabel:(UILabel *)label forIndexPath:(NSIndexPath *)indexPath;
- (NSNumber *)totalAmountForIndexPath:(NSIndexPath *)indexPath;
- (MoneyEntry *)entryForIndexPath:(NSIndexPath *)indexPath;
- (NSInteger)_tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
- (BOOL)_tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath;
- (void)_tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath;
- (void)_tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
- (UIView *)_tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section;

@end

@implementation IncomeExpenseLoanDataSource

- (void)loadEntriesWithCompletionBlock:(void(^)(void))completionBlock
{
    _sortedKeyForEntries = nil;
    
    NSPredicate *predicate = [self predicateWithBalanceSheetViewControllerType:BalanceSheetViewControllerType_Report];
    [[BalanceSheetDBController sharedInstance] loadEntriesFromBackgroundContextWithPredicate:predicate withCompletionBlock:^(NSArray *results) {
        
        [self parseResultsArray:results];
        
        if (completionBlock)
        {
            completionBlock();
        }
    }];
}

- (void)parseResultsArray:(NSArray *)results
{
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithCapacity:results.count];
    
    for (MoneyEntry *entry in results)
    {
        NSString *key = [self parseKeyFromMoneyEntry:entry];
        NSMutableArray *list = [dictionary[key] mutableCopy];
        if (!list)
        {
            list = [[NSMutableArray alloc] init];
        }
        
        [list addObject:entry];
        [dictionary setObject:list forKey:key];
    }
    
    _dicOfEntries = dictionary;
}

- (NSString *)parseKeyFromMoneyEntry:(MoneyEntry *)entry
{
    if (entry)
    {
        NSArray *entryArray = [entry.name componentsSeparatedByString:@"loan"];
        if (entryArray.count > 0 && entryArray.count <= 2)
        {
            NSArray* words = [entryArray[0] componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            return [words componentsJoinedByString:@""];
        }
    }
    return nil;
}

- (NSArray *)sortedKeyForEntries
{
    if (!_sortedKeyForEntries)
    {
        _sortedKeyForEntries = [self.dicOfEntries.allKeys sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
            return obj1 < obj2;
        }];
    }
    return _sortedKeyForEntries;
}

- (NSPredicate *)predicateWithBalanceSheetViewControllerType:(BalanceSheetViewControllerType)viewControllerType
{
    NSPredicate *predicate = nil;
    NSString *amountFilter = nil;
    NSString *textFilter = nil;
    
    switch (viewControllerType) {
        case BalanceSheetViewControllerType_Income:
        {
            amountFilter = @"amount >= 0";
            break;
        }
        case BalanceSheetViewControllerType_Outcome:
        {
            amountFilter = @"amount < 0";
            break;
        }
            
        default:
        {
            break;
        }
    }
    
    textFilter = @"name CONTAINS[cd] %@";
    
    NSMutableArray *filtersArray = [[NSMutableArray alloc] initWithCapacity:2];
    if (amountFilter) [filtersArray addObject:amountFilter];
    [filtersArray addObject:textFilter];
    
    NSString *filter = [filtersArray componentsJoinedByString:@" AND "];
    
    if (textFilter)
    {
        predicate = [NSPredicate predicateWithFormat:filter, @"loan"];
    }
    else if (amountFilter)
    {
        predicate = [NSPredicate predicateWithFormat:filter];
    }
    
    return predicate;
}

#pragma mark - abstract implementation
- (void)fillAmountLabel:(UILabel *)label forIndexPath:(NSIndexPath *)indexPath
{
    MoneyEntry *entry = [self entryForIndexPath:indexPath];
    label.text = [NSString stringWithFormat:@"%@", [[entry.amount unarchive] standardFormat]];
}

- (NSInteger)_numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.dicOfEntries.allKeys.count;
}

#pragma mark - abstract UITableViewDelegate helper
- (NSInteger)_tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString *key = self.sortedKeyForEntries[section];
    NSInteger count = ((NSArray *)[self.dicOfEntries objectForKey:key]).count;
    return count > 0? count + 1 : count;
}

- (BOOL)_tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

- (void)_tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
}

- (void)_tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

- (UIView *)_tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSString *key = self.sortedKeyForEntries[section];
    
    ReportSectionHeaderView *sectionHeader = (ReportSectionHeaderView *)[tableView dequeueReusableHeaderFooterViewWithIdentifier:[ReportSectionHeaderView uniqueIdentifier]];
    sectionHeader.headerLabel.text = key;
    return sectionHeader;

}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}


#pragma mark - abstract
- (NSNumber *)totalAmountForIndexPath:(NSIndexPath *)indexPath
{
    NSString *key = self.sortedKeyForEntries[indexPath.section];
    __block NSInteger total = 0;
    [((NSArray *)[self.dicOfEntries objectForKey:key]) enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        MoneyEntry *entry = (MoneyEntry *)obj;
        total += [entry.amount integerValue];
    }];
    return [@(total) unarchive];
}

- (MoneyEntry *)entryForIndexPath:(NSIndexPath *)indexPath
{
    NSString *key = self.sortedKeyForEntries[indexPath.section];
    NSArray *list = (NSArray *)[self.dicOfEntries objectForKey:key];
    if (indexPath.row < list.count)
    {
        return [list objectAtIndex:indexPath.row];
    }
    else
    {
        return nil;
    }
}

@end
