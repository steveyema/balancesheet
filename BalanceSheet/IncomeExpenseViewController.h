//
//  IncomeOutcomeBaseTableViewController.h
//  BalanceSheet
//
//  Created by Ye Ma on 2016-06-02.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "BaseTableViewController.h"
#import "IncomeExpenseDataSource.h"

@interface IncomeExpenseViewController : BaseTableViewController

@property (nonatomic, readonly, strong) IncomeExpenseDataSource *dataSource;

@end
