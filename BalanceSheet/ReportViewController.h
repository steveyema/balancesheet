//
//  ReportViewController.h
//  BalanceSheet
//
//  Created by Ye Ma on 2016-06-01.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewController.h"
#import "ReportDataSource.h"

@interface ReportViewController : BaseTableViewController

@property(nonatomic, assign) BalanceSheetDataSourceType balanceSheetDataSourceType;
@property(nonatomic, strong) ReportDataSource *dataSource;

@end
