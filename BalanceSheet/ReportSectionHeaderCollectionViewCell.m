//
//  ReportSectionHeaderCollectionViewCell.m
//  BalanceSheet
//
//  Created by Ye Ma on 2016-08-02.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "ReportSectionHeaderCollectionViewCell.h"

@interface ReportSectionHeaderCollectionViewCell()

@property(nonatomic, strong) UILabel *titleLabel;

@end

@implementation ReportSectionHeaderCollectionViewCell

+ (NSString *)reuseIdentifier
{
    return @"ReportSectionHeaderCollectionViewCell";
}

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        UILabel *label = self.titleLabel;
        
        [self addSubview:label];
        
//        [self setBackgroundColor:[UIColor orangeColor]];
        
        NSDictionary *views = NSDictionaryOfVariableBindings(label);
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[label]|" options:0 metrics:nil views:views]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[label]|" options:0 metrics:nil views:views]];
    }
    return self;
}

- (UILabel *)titleLabel
{
    if (!_titleLabel)
    {
        _titleLabel = [[UILabel alloc] initWithFrame:self.frame];
        [_titleLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
        
    }
    return _titleLabel;
}

@end
