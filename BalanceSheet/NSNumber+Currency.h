//
//  NSNumber+Currency.h
//  BalanceSheet
//
//  Created by Ye Ma on 2016-06-02.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumber (Currency)

- (NSString *)standardFormat;
+ (NSNumber *)unFormat:(NSString *)inputString;
- (NSNumber *)archive;
- (NSNumber *)unarchive;
- (NSNumber *)unarchivedPositive;

@end
