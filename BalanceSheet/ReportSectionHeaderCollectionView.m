//
//  ReportSectionHeaderCollectionView.m
//  BalanceSheet
//
//  Created by Ye Ma on 2016-08-02.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "ReportSectionHeaderCollectionView.h"
#import "ReportSectionHeaderDataSource.h"

@interface ReportSectionHeaderCollectionView()

@property(nonatomic, strong) UICollectionView *collectioView;
@property(nonatomic, strong) ReportSectionHeaderDataSource *dataSource;

@end

@implementation ReportSectionHeaderCollectionView

+(NSString *)uniqueIdentifier
{
    return @"ReportSectionHeaderCollectionView";
}

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithReuseIdentifier:reuseIdentifier])
    {
        UICollectionView *view = self.collectioView;
        
        [self.contentView addSubview:view];
        
        NSDictionary *views = NSDictionaryOfVariableBindings(view);
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:views]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:views]];
    }
    return self;
}

- (void)initDataSourceWithDelegate:(id<ReportSectionHeaderDataSourceDelegate>)delegate
{
    self.dataSource = [[ReportSectionHeaderDataSource alloc] initWithCollectionView:self.collectioView andDelegate:delegate];
    [self.collectioView reloadData];
}

- (UICollectionView *)collectioView
{
    if (!_collectioView)
    {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        _collectioView = [[UICollectionView alloc] initWithFrame:self.frame collectionViewLayout:flowLayout];
        [_collectioView setBackgroundColor:[UIColor clearColor]];
        [_collectioView setTranslatesAutoresizingMaskIntoConstraints:NO];
        
    }
    return _collectioView;
}


@end
