//
//  ReportSectionHeaderView.h
//  BalanceSheet
//
//  Created by Ye Ma on 2016-06-02.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReportSectionHeaderView : UITableViewHeaderFooterView

@property(nonatomic, strong) UILabel *headerLabel;

+(NSString *)uniqueIdentifier;

@end
