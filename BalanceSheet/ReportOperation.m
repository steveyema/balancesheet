//
//  ReportOperation.m
//  BalanceSheet
//
//  Created by Ye Ma on 2016-06-10.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "ReportOperation.h"
#import "ReportEntry.h"
#import "MoneyEntry+Reoccur.h"
#import "ReportOperationWithTotalAmount.h"
#import "ReportOperationForBreakdown.h"
#import "StateEntry.h"

@implementation ReportOperation

+ (id)operationWithType:(ReportOperationType)type andStateEntry:(StateEntry *)stateEntry
{
    ReportOperation *operation = nil;
    
    if (type == ReportOperationType_Basic)
    {
        operation= [[ReportOperation alloc] initWithStateEntry:stateEntry];
    }
    else if (type == ReportOperationType_Detail)
    {
        operation= [[ReportOperationWithTotalAmount alloc] initWithStateEntry:stateEntry];
    }
    else
    {
        operation = [[ReportOperationForBreakdown alloc] initWithStateEntry:stateEntry];
    }
    
    return operation;
}

-(id)initWithStateEntry:(StateEntry *)stateEntry
{
    if (self = [super init])
    {
        _stateEntry = stateEntry;
    }
    return self;
}

-(void)main
{
    NSMutableArray *results = [[NSMutableArray alloc] init];
    
    if (self.cancelled)
    {
        return;
    }
    
    ReportEntry *reportEntry = (ReportEntry *)self.stateEntry;
    
    NSArray *results1 = [self stateEntriesBetweenStartDate:reportEntry.startDate andEndDate:reportEntry.endDate];
    [results addObjectsFromArray:results1];
    
    if (self.cancelled)
    {
        return;
    }
    
    NSArray *results2 = [self reoccurMoneyEntriesToInstances];
    
    if (self.cancelled)
    {
        return;
    }
    
    [results2 enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        MoneyEntry *moneyEntry = (MoneyEntry *)obj;
        NSArray *result = [moneyEntry reoccurInstanceFromStartDate:reportEntry.startDate tillEndDate:reportEntry.endDate];
        NSMutableArray *_result = [[NSMutableArray alloc] initWithCapacity:result.count];
        //remove overwritten entries
        [result enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if (![self overwrittenEntry:obj inResult:results1])
            {
                [_result addObject:obj];
            }
        }];
        [results addObjectsFromArray:_result];
    }];
    
    reportEntry.detailInstances = [results copy];
    reportEntry.state = EntryState_Ready;
}

- (BOOL)overwrittenEntry:(MoneyEntry *)entry inResult:(NSArray *)result
{
    __block BOOL overwritten = NO;
    [result enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        MoneyEntry *tobeOverwritten = (MoneyEntry *)obj;
//        dispatch_async(dispatch_get_main_queue(), ^{
//            NSLog(@"%@ %@", tobeOverwritten.name, tobeOverwritten.datetime);
//        });
        if ([tobeOverwritten.name isEqualToString:entry.name] && [entry.datetime isSameAsDate:tobeOverwritten.datetime])
        {
            overwritten = YES;
        }
    }];
    return overwritten;
}

- (NSArray *)stateEntriesBetweenStartDate:(NSDate *)startDate andEndDate:(NSDate *)endDate
{
    ReportEntry *reportEntry = (ReportEntry *)self.stateEntry;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@" vdatetime <= %@ AND vdatetime >= %@ AND type == 0 AND (disabled == NO || disabled == NULL)", [reportEntry.endDate dateOnly], [reportEntry.startDate dateOnly]];
    
    return [[BalanceSheetDBController sharedInstance] loadEntriesFromBackgroundContextWithPredicate:predicate];
}

- (NSArray *)reoccurMoneyEntriesToInstances
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"type > 0 AND (disabled == NULL || disabled == NO)"];
    
    return [[BalanceSheetDBController sharedInstance] loadEntriesFromMainContextWithPredicate:predicate];
    

}

@end

