//
//  BalanceSheetTabDataSource.h
//  BalanceSheet
//
//  Created by Ye Ma on 2016-06-12.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "BalanceSheetDataSource.h"
#import "MainTabBarController.h"

@interface BalanceSheetTabDataSource : BalanceSheetDataSource

@property(nonatomic, assign) BalanceSheetTab balanceSheetTab;
@property(nonatomic, assign) BalanceSheetViewControllerType viewControllerType;
@property(nonatomic, strong) NSString *viewControllerTypeString;

@end
