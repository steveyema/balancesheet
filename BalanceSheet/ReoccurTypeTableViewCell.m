//
//  ReoccurTypeTableViewCell.m
//  BalanceSheet
//
//  Created by Ye Ma on 2016-06-03.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "ReoccurTypeTableViewCell.h"

@implementation ReoccurTypeTableViewCell

+ (NSString *)uniqueIdentifier
{
    return @"ReoccurTypeTableViewCell";
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
}

- (UILabel *)titleLabel
{
    if (!_titleLabel)
    {
        UILabel *label = [[UILabel alloc] init];
        
        label.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self addSubview:label];
        
        id views = NSDictionaryOfVariableBindings(label);
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-5-[label]|" options:0 metrics:nil views:views]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[label]|" options:0 metrics:nil views:views]];
        
        _titleLabel = label;
    }
    return _titleLabel;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
