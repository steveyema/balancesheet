//
//  BaseTableViewController.h
//  BalanceSheet
//
//  Created by Ye Ma on 2016-06-01.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IncomeExpenseDataSource.h"

@interface BaseTableViewController : UIViewController

@property(nonatomic, strong) UITableView *tableView;

@end
