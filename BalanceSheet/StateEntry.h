//
//  Entry.h
//  BalanceSheet
//
//  Created by Ye Ma on 2016-07-03.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, EntryState) {
    EntryState_New = 0,
    EntryState_Ready
};

@interface StateEntry : NSObject

@property(nonatomic, assign) EntryState state;

@end
