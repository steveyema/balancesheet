//
//  NSDate+Formatter.h
//  BalanceSheet
//
//  Created by Ye Ma on 2016-06-02.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Formatter)

- (NSString *)shortDate;
- (NSDate *)dateOnly;
- (BOOL)isOlderThanDate:(NSDate *)date2;
- (BOOL)isSameAsDate:(NSDate *)date2;
- (NSDate *)adjustByNumberOfWeek:(NSInteger)numberOfWeek keepOriginalDate:(BOOL)keepOriginalDate;
- (NSDate *)adjustByNumberOfMonth:(NSInteger)numberOfMonth keepOriginalDate:(BOOL)keepOriginalDate;
- (NSDate *)adjustByNumberOfYear:(NSInteger)numberOfYear keepOriginalDate:(BOOL)keepOriginalDate;

@end
