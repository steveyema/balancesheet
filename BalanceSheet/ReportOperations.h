//
//  ReportOperations.h
//  BalanceSheet
//
//  Created by Ye Ma on 2016-06-10.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReportOperations : NSObject

@property(nonatomic, strong) NSOperationQueue *queue;
@property(nonatomic, strong) NSMutableDictionary *fetchingInProgress;

@end
