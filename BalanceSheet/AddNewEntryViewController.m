//
//  AddNewEntryViewController.m
//  BalanceSheet
//
//  Created by Ye Ma on 2016-05-31.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "AddNewEntryViewController.h"
#import "AddNewEntrySelectionViewController.h"
#import "ReoccurViewController.h"
#import "IncomeExpenseBreakDownViewController.h"

@interface AddNewEntryViewController()<ReoccurViewControllerDeleate, UITextFieldDelegate, AddNewEntrySelectionViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIButton *doneButton;
@property (weak, nonatomic) IBOutlet UITextField *nameText;
@property (weak, nonatomic) IBOutlet UITextField *amountText;
@property (weak, nonatomic) IBOutlet UITextField *dateText;
@property (weak, nonatomic) IBOutlet UISwitch *incomeSwitch;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UIButton *dateButton;
@property (weak, nonatomic) IBOutlet UISwitch *reoccurSwitch;
@property (weak, nonatomic) IBOutlet UILabel *reoccurTypeLabel;
@property (weak, nonatomic) IBOutlet UISwitch *diabledSwitch;
//@property (weak, nonatomic) IBOutlet UIButton *breakdownButton;


- (IBAction)doneButtonClicked:(id)sender;
- (IBAction)cancelButtonClicked:(id)sender;
- (IBAction)dateButtonTapped:(id)sender;
- (IBAction)reoccurTapped:(id)sender;
//Deprecated
//- (IBAction)handleBreakdownPressed:(id)sender;

@property(nonatomic, strong) NSArray *titleArray;
@property(nonatomic, strong) NSDateFormatter *dateFormatter;

@end

@implementation AddNewEntryViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.incomeSwitch addTarget:self action:@selector(incomeSwitchTapped:) forControlEvents:UIControlEventValueChanged];
    [self.reoccurSwitch addTarget:self action:@selector(reoccurSwitchTapped:) forControlEvents:UIControlEventValueChanged];
    
    [self.datePicker addTarget:self action:@selector(datePickerChanged:) forControlEvents:UIControlEventValueChanged];
    
    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped:)]];
    
    self.amountText.delegate = self;
    
    [self.dataSource preEntryCreation:self];
    
    [self viewWillbeEditing];
    
}

- (NSArray *)titleArray
{
    if (!_titleArray)
    {
        static dispatch_once_t onceToken;
        static NSArray *array = nil;
        dispatch_once(&onceToken, ^{
            array = @[@"One time",
                      @"Daily",
                      @"Weekly",
                      @"Every Two Weeks",
                      @"Every Half Month",
                      @"Monthly",
                      @"Every Two Month"];
        });
        _titleArray = array;
    }
    return _titleArray;
}

- (void)viewWillbeEditing
{
//Deprecated
//    self.breakdownButton.hidden = YES;
    
    id obj = [self.dataSource prepareEntryForEdit];
    if ([obj isKindOfClass:[MoneyEntry class]])
    {
        MoneyEntry *entry = (MoneyEntry *)obj;
        //Deprecated
//        if ([entry.type intValue] > 0)
//        {
//            self.breakdownButton.hidden = NO;
//            
//        }
        self.nameText.text = entry.name;
        self.dateText.text = entry.vdatetime? [entry.vdatetime shortDate] : [entry.datetime shortDate];
        self.amountText.text = entry.amount? [[entry.amount unarchivedPositive] standardFormat] : @"0";
        self.incomeSwitch.on = [entry.amount integerValue] >= 0;
        self.reoccurSwitch.on = [entry.type integerValue] != BalanceSheetEntryType_OneTime;
        self.reoccurTypeLabel.text = self.titleArray[[entry.type integerValue]];
        self.reoccurTypeLabel.tag = [entry.type integerValue];
        self.diabledSwitch.on = [entry.disabled boolValue];
    }
    else
    {
        self.incomeSwitch.on = self.dataSource.viewControllerType == BalanceSheetViewControllerType_Income;
    }
}



- (NSDateFormatter *)dateFormatter
{
    if (!_dateFormatter)
    {
        _dateFormatter = [[NSDateFormatter alloc] init];
        [_dateFormatter setDateFormat:@"yyyy-MM-dd"];
    }
    return _dateFormatter;
}

#pragma mark - Action
- (void)viewTapped:(id)sender
{
    [self.nameText resignFirstResponder];
    [self.amountText resignFirstResponder];
    [self.dateText resignFirstResponder];
}

- (void)incomeSwitchTapped:(id)sender
{
    NSLog(@"income switch: %d", self.incomeSwitch.on);
}

- (void)reoccurSwitchTapped:(id)sender
{
    NSLog(@"reoccur switch: %d", self.incomeSwitch.on);
}

- (void)datePickerChanged:(id)sender
{
    self.dateText.text = [self.datePicker.date shortDate];
    [self.datePicker setHidden:YES];
}

- (IBAction)dateButtonTapped:(id)sender
{
    [self.datePicker setHidden:!self.datePicker.hidden];
    [self.view bringSubviewToFront:self.datePicker];
}

- (IBAction)reoccurTapped:(id)sender
{
    ReoccurViewController *viewController = [ReoccurViewController viewControllerWithDelegate:self];
    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma mark - AddNewEntrySelectionViewControllerDelegate method
- (void)doneButtonClicked:(id)sender
{
    NSString *name = self.nameText.text;
    double doubleAmount = [[NSNumber unFormat:self.amountText.text] doubleValue];
    if (self.incomeSwitch && !self.incomeSwitch.on)
    {
        doubleAmount = -doubleAmount;
    }
    
    BalanceSheetEntryType entryType = BalanceSheetEntryType_OneTime;
    if (self.reoccurSwitch.on)
    {
        entryType = self.reoccurTypeLabel.tag;
    }
    
    NSNumber *amount = [@(doubleAmount) archive];
    NSDate *date = (self.dateText.text && ![self.dateText.text isEqualToString:@""])? [self.dateFormatter dateFromString:self.dateText.text] : [NSDate date];
    
    BOOL disabled = self.diabledSwitch.on;
    
    NSDictionary *entry = @{
                            MONEYENTRY_NAME : name,
                            MONEYENTRY_AMOUNT : amount,
                            MONEYENTRY_TYPE : @(entryType),
                            MONEYENTRY_DATETIME : date,
                            MONEYENTRY_DISABLED : @(disabled)
                            };

    if (self.editMode)
    {
        id obj = [self.dataSource prepareEntryForEdit];
        if ([obj isKindOfClass:[MoneyEntry class]])
        {
            if ([self.dataSource respondsToSelector:@selector(editExistingEntry:)])
            {
                [self.dataSource editExistingEntry:entry];
                
            }
        }
    }
    else
    {
        if ([self.dataSource respondsToSelector:@selector(addNewEntry:)])
        {
            [self.dataSource addNewEntry:entry];
            
        }
    }
    
    [self.dataSource postEntryForEdit];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)cancelButtonClicked:(id)sender
{
    [self.dataSource postEntryForEdit];
    [self dismissViewControllerAnimated:YES completion:nil];
}

//Deprecated
//- (IBAction)handleBreakdownPressed:(id)sender {
//    IncomeExpenseBreakDownViewController *viewController = [IncomeExpenseBreakDownViewController instanceWithDataSource:self.dataSource];
//    [self.navigationController pushViewController:viewController animated:YES];
//}

#pragma mark - ReoccurViewControllerDeleate
- (NSArray *)reoccurTitleArray
{
    return self.titleArray;
}

- (void)doneSelectReoccurType:(BalanceSheetEntryType)balanceSheetEntryType withDisplayText:(NSString *)displayText
{
    self.reoccurTypeLabel.text = displayText;
    self.reoccurTypeLabel.tag = balanceSheetEntryType;
    
    self.reoccurSwitch.on = balanceSheetEntryType != BalanceSheetEntryType_OneTime;
}

#pragma mark - UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    NSLog(@"begin editing");
    
    if (textField == self.amountText)
    {
        textField.text = (self.amountText.text && ![self.amountText.text isEqualToString:@""]) ? [NSString stringWithFormat:@"%@", [NSNumber unFormat:self.amountText.text]] : @"";
        textField.text = [textField.text isEqualToString:@"0"]? @"" : textField.text;
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    NSLog(@"finished editing");
    if (textField == self.amountText)
    {
        textField.text = [@([self.amountText.text doubleValue]) standardFormat];
    }
}

@end
