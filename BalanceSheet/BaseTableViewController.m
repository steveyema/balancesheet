//
//  BaseTableViewController.m
//  BalanceSheet
//
//  Created by Ye Ma on 2016-06-01.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "BaseTableViewController.h"
#import "IncomeExpenseDataSource.h"

@interface BaseTableViewController ()

@end

@implementation BaseTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    CGRect frame = self.view.bounds;

    UITableView *tableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStylePlain];
    tableView.translatesAutoresizingMaskIntoConstraints = NO;
    tableView.tableFooterView = [[UIView alloc] init];
    self.tableView = tableView;
    
    tableView.layoutMargins = UIEdgeInsetsZero;
    tableView.separatorInset = UIEdgeInsetsZero;
    
    [self.view addSubview:tableView];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if (IS_IPAD())
    {
        // Support all interface orientations.
        return UIInterfaceOrientationMaskAll;
    }
    
    else
    {
        // Make sure we only support portrait orientation.
        return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskLandscapeRight;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
