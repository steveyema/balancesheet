//
//  IncomeExpenseBreakDownViewController.h
//  BalanceSheet
//
//  Created by Ye Ma on 2016-06-12.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewController.h"

@interface IncomeExpenseBreakDownViewController : BaseTableViewController

+ (id)instanceWithDataSource:(BalanceSheetTabDataSource *)dataSource;

@end
