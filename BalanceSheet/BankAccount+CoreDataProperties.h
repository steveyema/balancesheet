//
//  BankAccount+CoreDataProperties.h
//  BalanceSheet
//
//  Created by Ye Ma on 2016-06-02.
//  Copyright © 2016 Ye Ma. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "BankAccount.h"

NS_ASSUME_NONNULL_BEGIN

@interface BankAccount (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSNumber *balance;
@property (nullable, nonatomic, retain) NSDate *datetime;

@end

NS_ASSUME_NONNULL_END
