//
//  ReoccurViewController.h
//  BalanceSheet
//
//  Created by Ye Ma on 2016-06-03.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MoneyEntry+Reoccur.h"

@protocol ReoccurViewControllerDeleate <NSObject>

- (NSArray *)reoccurTitleArray;
- (void)doneSelectReoccurType:(BalanceSheetEntryType)balanceSheetEntryType withDisplayText:(NSString *)displayText;

@end

@interface ReoccurViewController : UIViewController

@property(nonatomic, weak) id<ReoccurViewControllerDeleate> delegate;

+ (id)viewControllerWithDelegate:(id<ReoccurViewControllerDeleate>)delegate;

@end
