//
//  ReportDataSourceBase.m
//  BalanceSheet
//
//  Created by Ye Ma on 2016-07-03.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "ReportDataSourceBase.h"
#import "ReportOperation.h"
#import "ReportOperations.h"
#import "ReportEntry.h"
#import "StateEntry.h"

@interface ReportDataSourceBase()<UIScrollViewDelegate>

@property(nonatomic, strong) ReportOperations *balanceSheetOperations;

@end

@implementation ReportDataSourceBase

-(id)init
{
    if (self = [super init])
    {
        _balanceSheetOperations = [[ReportOperations alloc] init];
        _page = 1;
        
        _reportEntries = [[NSMutableDictionary alloc] init];
        _reportOperationType = ReportOperationType_Basic;
        
        
    }
    return self;
}

-(void)startOperationForReportEntry:(StateEntry *)stateEntry atIndexPath:(NSIndexPath *)indexPath
{
    if (stateEntry.state == EntryState_Ready) return;
    
    ReportOperation *operation = self.balanceSheetOperations.fetchingInProgress[indexPath];
    
    if (operation)
    {
        return;
    }
    
    operation = [ReportOperation operationWithType:self.reportOperationType andStateEntry:stateEntry];
    __weak ReportOperation *weakOperation = operation;
    operation.completionBlock = ^{
        
        if (weakOperation.isCancelled)
        {
            return;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.balanceSheetOperations.fetchingInProgress removeObjectForKey:indexPath];
            if ([self.delegate respondsToSelector:@selector(reloadRowsAtIndexPaths:withRowAnimation:)])
            {
                [self.delegate reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            }
        });
    };
    self.balanceSheetOperations.fetchingInProgress[indexPath] = operation;
    [self.balanceSheetOperations.queue addOperation:operation];
}

- (void)cancelOperations
{
    for (ReportOperation *operation in self.balanceSheetOperations.fetchingInProgress)
    {
        [operation cancel];
    }
}

- (void)resetReportEntries
{
    for (NSIndexPath *indexPath in self.reportEntries.allKeys)
    {
        ReportEntry *reportEntry = self.reportEntries[indexPath];
        reportEntry.state = EntryState_New;
    }
}

- (void)suspendAllOperations
{
    self.balanceSheetOperations.queue.suspended = YES;
}

- (void)resumeAllOperations
{
    self.balanceSheetOperations.queue.suspended = NO;
}

- (NSSet *)subtract:(NSSet *)set1 withSet:(NSSet *)set2
{
    NSMutableSet *result = [[NSMutableSet alloc] initWithCapacity:set1.count];
    [set1 enumerateObjectsUsingBlock:^(id  _Nonnull obj, BOOL * _Nonnull stop) {
        if (![set2 containsObject:obj])
        {
            [result addObject:obj];
        }
    }];
    return result;
}

- (void)loadReportOnScreen
{
    NSArray *pathArray = nil;
    if ([self.delegate respondsToSelector:@selector(indexPathsForVisibleRows)])
    {
        pathArray = [self.delegate indexPathsForVisibleRows];
    }
    
    //2
    NSSet *allPendingOperations = [NSSet setWithArray:self.balanceSheetOperations.fetchingInProgress.allKeys];
    
    NSSet *toBeCanceled = allPendingOperations;
    NSSet *visiblePaths = [NSSet setWithArray:pathArray];
    toBeCanceled = [self subtract:toBeCanceled withSet:visiblePaths];
    
    NSSet *toBeStarted = visiblePaths;
    toBeStarted = [self subtract:toBeStarted withSet:allPendingOperations];
    
    for (NSIndexPath *indexPath in toBeCanceled)
    {
        NSOperation *operation = (NSOperation *)self.balanceSheetOperations.fetchingInProgress[indexPath];
        [operation cancel];
        
        [self.balanceSheetOperations.fetchingInProgress removeObjectForKey:indexPath];
    }
    
    for (NSIndexPath *indexPath in toBeStarted)
    {
        ReportEntry *reportEntry = self.reportEntries[indexPath];
        if (reportEntry)
        {
            [self startOperationForReportEntry:reportEntry atIndexPath:indexPath];
        }
    }
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self suspendAllOperations];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!decelerate)
    {
        [self loadReportOnScreen];
        [self resumeAllOperations];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self loadReportOnScreen];
    [self resumeAllOperations];
}



@end
