//
//  BalanceSheetDataSource.h
//  BalanceSheet
//
//  Created by Ye Ma on 2016-06-02.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ReportDataSourceBase.h"

@class UITableView;

@interface ReportDataSource : ReportDataSourceBase

+(id)dataSourceWithBalanceSheetDataSourceType:(BalanceSheetDataSourceType)balanceSheetDataSourceType andDelegate:(id<BalanceSheetDataSourceDelegate>)delegate andTableView:(UITableView *)tableView;

@end
