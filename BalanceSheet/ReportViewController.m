//
//  ReportViewController.m
//  BalanceSheet
//
//  Created by Ye Ma on 2016-06-01.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "ReportViewController.h"
#import "IncomeExpenseDataSource.h"
#import "AddNewEntryViewController.h"
#import "AddNewEntrySelectionViewController.h"
#import "BalanceSheetBreakDownViewController.h"

@interface ReportViewController ()<BalanceSheetDataSourceDelegate>
- (IBAction)editTapped:(id)sender;
@property (nonatomic, strong) NSIndexPath *selectedIndexPath;

@end

@implementation ReportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"Report";
    
    id bottomGuide = self.bottomLayoutGuide;
    id tableView = self.tableView;
    
    NSDictionary *views = NSDictionaryOfVariableBindings(tableView,bottomGuide);
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[tableView][bottomGuide]|" options:0 metrics:nil views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[tableView]|" options:0 metrics:nil views:views]];

    BalanceSheetDataSourceType type = [BalanceSheetApplication sharedInstance].activeBalanceSheetDataSourceType;
    self.dataSource = [ReportDataSource dataSourceWithBalanceSheetDataSourceType:type andDelegate:self andTableView:self.tableView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.dataSource viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    UIViewController *viewController = segue.destinationViewController;
    if ([viewController isKindOfClass:[UINavigationController class]])
    {
        viewController = ((UINavigationController *)viewController).topViewController;
    }
    
    if ([viewController isKindOfClass:[AddNewEntrySelectionViewController class]])
    {
        AddNewEntrySelectionViewController *addNewEntrySelectionViewController = (AddNewEntrySelectionViewController *)viewController;
        addNewEntrySelectionViewController.dataSource = self.dataSource;
    }
}

- (IBAction)editTapped:(id)sender {
    [self.tableView setEditing:!self.tableView.editing];
}

#pragma mark - BalanceSheetDataSourceDelegate
- (void)updateUI
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}

- (void)reloadRowsAtIndexPaths:(NSArray<NSIndexPath *> *)indexPaths withRowAnimation:(UITableViewRowAnimation)animation
{
    [self.tableView beginUpdates];
    [self.tableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
    [self.tableView endUpdates];
}

- (NSArray *)indexPathsForVisibleRows
{
    return [self.tableView indexPathsForVisibleRows];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        //TODO edit bank record
    }
    else
    {
        self.selectedIndexPath = indexPath;
        UIViewController *viewController = [BalanceSheetBreakDownViewController viewControllerWithDelegate:self];
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

- (id)selectedItem
{
    if (self.selectedIndexPath)
    {
        return self.dataSource.reportEntries[self.selectedIndexPath];
    }
    else
    {
        return nil;
    }
}

@end
