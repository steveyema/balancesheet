//
//  BalanceSheetDataSource.m
//  BalanceSheet
//
//  Created by Ye Ma on 2016-06-05.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "BalanceSheetDataSource.h"
#import "MainTabBarController.h"

@implementation BalanceSheetDataSource

- (id)init
{
    if (self = [super init])
    {
        
    }
    return self;
}

#pragma mark - abstract
- (void)viewDidLoad
{
}

- (void)viewWillAppear:(BOOL)animated
{
    [NSException raise:@"Error" format:@"abstract method viewWillAppear: is not implemented in %@", self];
}

- (void)updateDataScreen
{
    [NSException raise:@"Error" format:@"abstract method updateDataScreen is not implemented in %@", self];

}

- (void)preEntryCreation:(UIViewController *)viewController
{
}

- (id)prepareEntryForEdit
{
    return nil;
}

- (void)postEntryForEdit
{
}

- (void)addNewEntry:(NSDictionary *)entryDic
{
    [NSException raise:@"Error" format:@"abstract method addNewEntry: is not implemented in %@", self];
}

- (void)editExistingEntry:(NSDictionary *)entry
{
    [NSException raise:@"Error" format:@"abstract method editExistingEntry: is not implemented in %@", self];
}

@end
