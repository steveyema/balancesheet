//
//  ReportSectionHeaderView.m
//  BalanceSheet
//
//  Created by Ye Ma on 2016-06-02.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "ReportSectionHeaderView.h"

@implementation ReportSectionHeaderView

+(NSString *)uniqueIdentifier
{
    return @"ReportSectionHeaderView";
}

- (UILabel *)headerLabel
{
    if (!_headerLabel)
    {
        UILabel *label = [[UILabel alloc] initWithFrame:self.frame];
        label.translatesAutoresizingMaskIntoConstraints = NO;
        
        _headerLabel = label;
        [self addSubview:label];
    }
    return _headerLabel;
}

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithReuseIdentifier:reuseIdentifier])
    {
        id label = self.headerLabel;
        NSDictionary *views = NSDictionaryOfVariableBindings(label);
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[label]|" options:0 metrics:nil views:views]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[label]|" options:0 metrics:nil views:views]];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
