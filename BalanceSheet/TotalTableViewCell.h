//
//  TotalTableViewCell.h
//  BalanceSheet
//
//  Created by Ye Ma on 2016-06-02.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TotalTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *totalLabel;

@end
