//
//  BalanceSheetBreakDownDataSource.h
//  BalanceSheet
//
//  Created by Ye Ma on 2016-06-10.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "BalanceSheetTabDataSource.h"

@interface BalanceSheetBreakDownDataSource : BalanceSheetTabDataSource<UITableViewDelegate, UITableViewDataSource>

+ (id)dataSourceWithTableView:(UITableView *)tableView andDelegate:(id<BalanceSheetDataSourceDelegate>)delegate andCompletionBlock:(void(^)(void))completionBlock;

@end
