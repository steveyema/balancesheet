//
//  UIColor+Scheme.h
//  BalanceSheet
//
//  Created by Ye Ma on 2016-07-15.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Scheme)

+ (UIColor *)clearBackgroundColor;
+ (UIColor *)highlightedCellBackgroundColor;
+ (UIColor *)disabledCellBackgroundColor;
+ (UIColor *)lightHeaderBackgroundColor;
+ (UIColor *)whiteTextColor;


@end
