//
//  FileHelper.h
//  BalanceSheet
//
//  Created by Ye Ma on 2016-05-31.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FileHelper : NSObject

NSString *pathInAppDirectory(NSSearchPathDirectory directory, NSString *fileName, BOOL createDirectoryIfMissing);

@end
