//
//  Constant.h
//  BalanceSheet
//
//  Created by Ye Ma on 2016-05-31.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import <UIKit/UIKit.h>

#ifndef Constant_h
#define Constant_h

#define IS_IPAD() (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

#define BALANCESHEETDBFILE @"BalanceSheetDB.data"

#define BANK_ACCOUNT_LIMIT 5000

#define ACTIVE_BALANCE_SHEET_TAB_KEY @"ActiveBalanceSheetTabKey"
#define ACTIVE_BALANCE_SHEET_DATA_SOURCE_TYPE_KEY @"ActiveBalanceSheetDataSourceTypeKey"

#define MONEYENTRY @"MoneyEntry"
#define MONEYENTRY_NAME @"name"
#define MONEYENTRY_DATETIME @"datetime"
#define MONEYENTRY_AMOUNT @"amount"
#define MONEYENTRY_TYPE @"type"
#define MONEYENTRY_DISABLED @"disabled"

#define BANKACCOUNT @"BankAccount"
#define BANKACCOUNTRECORD_NAME @"name"
#define BANKACCOUNTRECORD_BALANCE @"balance"
#define BANKACCOUNTRECORD_DATETIME @"datetime"

#define NOTIFICATION_ADD_NEW_ENTRY @"NOTIFICATION_ADD_NEW_ENTRY"

#define REPORT_ENTRY_CELL_IDENTIFIER @"ReportEntryCell"
#define REPORT_LOADING_CELL_IDENTIFIER @"ReportLoadingCell"

#define PageSize 100

@protocol BalanceSheetDataSourceDelegate <NSObject>

@optional
- (void)updateUI;
- (void)reloadRowsAtIndexPaths:(NSArray<NSIndexPath *> *)indexPaths withRowAnimation:(UITableViewRowAnimation)animation;
- (NSArray *)indexPathsForVisibleRows;
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
- (id)selectedItem;

@end

typedef  NS_ENUM(NSInteger, BalanceSheetViewControllerType) {
    BalanceSheetViewControllerType_Income = 0,
    BalanceSheetViewControllerType_Outcome,
    BalanceSheetViewControllerType_Report
};

#endif /* Constant_h */
