//
//  IncomeExpenseBaseDataSource.m
//  BalanceSheet
//
//  Created by Ye Ma on 2016-07-15.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "IncomeExpenseBaseDataSource.h"
#import "IncomeExpenseLoanDataSource.h"
#import "IncomeExpenseDataSource.h"
#import "MoneyEntryTableViewCell.h"
#import "MoneyEntry+Reoccur.h"
#import "TotalTableViewCell.h"
#import "IncomeExpenseBreakDownViewController.h"
#import "ReportSectionHeaderView.h"

@interface IncomeExpenseBaseDataSource()<UITabBarDelegate, UITableViewDataSource>

@property(nonatomic, strong) MoneyEntry *selectedEntry;

@end

@implementation IncomeExpenseBaseDataSource

+ (id)dataSourceFor:(IncomeExpenseDataSourceType)incomeExpenseDataSourceType WithDelegate:(id<BalanceSheetDataSourceDelegate>)delegate andTableView:(UITableView *)tableView
{
    BalanceSheetDataSource *dataSource = nil;
    
    if (incomeExpenseDataSourceType == IncomeExpenseDataSourceType_Regular)
    {
        dataSource = [[IncomeExpenseDataSource alloc] initWithDelegate:delegate andTableView:
         tableView];
    }
    else
    {
        dataSource = [[IncomeExpenseLoanDataSource alloc] initWithDelegate:delegate andTableView:
         tableView];
    }
    dataSource.delegate = delegate;
    return dataSource;
}

- (id)initWithDelegate:(id<BalanceSheetDataSourceDelegate>)delegate andTableView:(UITableView *)tableView
{
    if (self = [super init])
    {
        tableView.delegate = self;
        tableView.dataSource = self;
        self.delegate = delegate;
        
        [tableView registerNib:[UINib nibWithNibName:@"MoneyEntry" bundle:nil] forCellReuseIdentifier:@"MoneyEntryCell"];
        [tableView registerNib:[UINib nibWithNibName:@"TotalCell" bundle:nil] forCellReuseIdentifier:@"TotalCell"];
        
        [tableView registerClass:[ReportSectionHeaderView class] forHeaderFooterViewReuseIdentifier:[ReportSectionHeaderView uniqueIdentifier]];
        
        [self updateDataScreen];
        
        
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return [self _tableView:tableView heightForHeaderInSection:section];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [self _tableView:tableView viewForHeaderInSection:section];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self _numberOfSectionsInTableView:tableView];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self _tableView:tableView numberOfRowsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MoneyEntry *moneyEntry = [self entryForIndexPath:indexPath];
    if (moneyEntry)
    {
        MoneyEntryTableViewCell *cell = (MoneyEntryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"MoneyEntryCell"];
        cell.nameLabel.text = moneyEntry.name;
        [self fillAmountLabel:cell.amountLabel forIndexPath:indexPath];
        cell.dateLabel.text = [NSString stringWithFormat:@"%@", [moneyEntry.vdatetime shortDate]];
        cell.backgroundColor = [moneyEntry.disabled boolValue]? [UIColor disabledCellBackgroundColor] : [UIColor clearBackgroundColor];
        return cell;
    }
    else
    {
        TotalTableViewCell *cell = (TotalTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"TotalCell"];
        cell.totalLabel.text = [NSString stringWithFormat:@"%@", [[self totalAmountForIndexPath:indexPath] standardFormat]];
        return cell;
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self _tableView:tableView canEditRowAtIndexPath:indexPath];
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self _tableView:tableView commitEditingStyle:editingStyle forRowAtIndexPath:indexPath];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self _tableView:tableView didSelectRowAtIndexPath:indexPath];
    
    if (self.selectedEntry)
    {
        if ([self.delegate respondsToSelector:@selector(tableView:didSelectRowAtIndexPath:)])
        {
            [self.delegate tableView:tableView didSelectRowAtIndexPath:indexPath];
        }
    }
}

#pragma mark - abstract implementation
-(void)updateDataScreen
{
    [self loadEntriesWithCompletionBlock:^{
        //refresh UI
        if ([self.delegate respondsToSelector:@selector(updateUI)])
        {
            [self.delegate updateUI];
        }
    }];
}

- (void)loadEntriesWithCompletionBlock:(void(^)(void))completionBlock
{
    [NSException raise:@"Error" format:@"abstract method loadEntriesWithCompletionBlock is not implemented in %@", self];
}

- (id)prepareEntryForEdit
{
    return self.selectedEntry;
}

- (void)postEntryForEdit
{
    self.selectedEntry = nil;
}

- (BOOL)presentBreakdownActionSheetForIndexPath:(NSIndexPath *)indexPath FromViewController:(UIViewController *)viewController withSourceView:(UIView *)view
{
    [self _tableView:nil didSelectRowAtIndexPath:indexPath];
    
    if ([self.selectedEntry.type integerValue] == BalanceSheetEntryType_OneTime)
    {
        return NO;
    }
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *action = [UIAlertAction actionWithTitle:[NSString stringWithFormat:@"%@ breakdown", self.viewControllerTypeString] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //
        NSLog(@"show breakdown");
        IncomeExpenseBreakDownViewController *breakdownViewController = [IncomeExpenseBreakDownViewController instanceWithDataSource:self];
        [viewController.navigationController pushViewController:breakdownViewController animated:YES];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        //
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertController addAction:action];
    [alertController addAction:cancelAction];
    
    alertController.popoverPresentationController.sourceView = view;
    alertController.popoverPresentationController.sourceRect = CGRectMake(view.bounds.size.width / 2.0, view.bounds.size.height / 2.0, 1.0, 1.0);
    
    [viewController presentViewController:alertController animated:YES completion:nil];
    
    return YES;
}

#pragma mark - abstract UITableViewDelegate helper
- (NSInteger)_numberOfSectionsInTableView:(UITableView *)tableView
{
    [NSException raise:@"Error" format:@"_numberOfSectionsInTableView: was not implemented!"];
    return 0;
}

- (NSInteger)_tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    [NSException raise:@"Error" format:@"_tableView:numberOfRowsInSection: was not implemented!"];
    return 0;
}

- (BOOL)_tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    [NSException raise:@"Error" format:@"_tableView:canEditRowAtIndexPath: was not implemented!"];
    return NO;
}

- (void)_tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [NSException raise:@"Error" format:@"_tableView:commitEditingStyle: was not implemented!"];
}

- (void)_tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [NSException raise:@"Error" format:@"_tableView:didSelectRowAtIndexPath: was not implemented!"];
}

- (UIView *)_tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    [NSException raise:@"Error" format:@"_tableView:viewForHeaderInSection: was not implemented!"];
    return nil;
}

- (CGFloat)_tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    [NSException raise:@"Error" format:@"_tableView:heightForHeaderInSection: was not implemented!"];
    return 0;
}

#pragma mark - abstract
- (void)fillAmountLabel:(UILabel *)label forIndexPath:(NSIndexPath *)indexPath
{
    [NSException raise:@"Error" format:@"fillAmountLabel:forIndexPath: was not implemented!"];
}

- (NSNumber *)totalAmountForIndexPath:(NSIndexPath *)indexPath
{
    [NSException raise:@"Error" format:@"fillAmountLabel:withEntry: was not implemented!"];
    return @(0);
}

- (MoneyEntry *)entryForIndexPath:(NSIndexPath *)indexPath
{
    [NSException raise:@"Error" format:@"fillAmountLabel:withEntry: was not implemented!"];
    return nil;
}

@end
