//
//  IncomeExpenseBreakdownDataSource.h
//  BalanceSheet
//
//  Created by Ye Ma on 2016-07-03.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "ReportDataSourceBase.h"

@class MoneyStateEntry;

@interface IncomeExpenseBreakdownDataSource : ReportDataSourceBase

+(id)dataSourceWithDelegate:(id<BalanceSheetDataSourceDelegate>)delegate andTableView:(UITableView *)tableView;
- (MoneyStateEntry *)selectedMoneyStateEntry;

@end
