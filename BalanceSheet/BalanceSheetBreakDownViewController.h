//
//  BalanceSheetBreakDownViewController.h
//  BalanceSheet
//
//  Created by Ye Ma on 2016-06-10.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "BaseTableViewController.h"

@interface BalanceSheetBreakDownViewController : BaseTableViewController

@property(nonatomic, strong) BalanceSheetTabDataSource *dataSource;

+ (id)viewControllerWithDelegate:(id<BalanceSheetDataSourceDelegate>)delegate;

@end
