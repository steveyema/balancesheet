//
//  AddNewEntryViewController.h
//  BalanceSheet
//
//  Created by Ye Ma on 2016-05-31.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddNewEntryViewController : UIViewController

@property(nonatomic, assign) BOOL editMode;
@property(nonatomic, strong) BalanceSheetTabDataSource *dataSource;

@end
