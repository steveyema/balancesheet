//
//  MoneyEntryTableViewCell.m
//  BalanceSheet
//
//  Created by Ye Ma on 2016-05-31.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "MoneyEntryTableViewCell.h"

@interface MoneyEntryTableViewCell()

@end

@implementation MoneyEntryTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
