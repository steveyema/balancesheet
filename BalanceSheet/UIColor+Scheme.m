//
//  UIColor+Scheme.m
//  BalanceSheet
//
//  Created by Ye Ma on 2016-07-15.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "UIColor+Scheme.h"

@implementation UIColor (Scheme)

+ (UIColor *)clearBackgroundColor
{
    return [UIColor clearColor];
}

+ (UIColor *)highlightedCellBackgroundColor
{
    return [UIColor orangeColor];
}

+ (UIColor *)disabledCellBackgroundColor
{
    return [UIColor lightGrayColor];
}

+ (UIColor *)lightHeaderBackgroundColor
{
    return [UIColor lightGrayColor];
}

+ (UIColor *)whiteTextColor
{
    return [UIColor whiteColor];;
}

@end
