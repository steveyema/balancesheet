//
//  AddNewEntrySelectionViewController.m
//  BalanceSheet
//
//  Created by Ye Ma on 2016-06-03.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "AddNewEntrySelectionViewController.h"
#import "AddNewEntryViewController.h"

@interface AddNewEntrySelectionViewController ()
- (IBAction)handleDonePressed:(id)sender;
- (IBAction)handleCancelPressed:(id)sender;

@end

@implementation AddNewEntrySelectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    
    AddNewEntryViewController *addNewEntryViewController = nil;
    if (self.dataSource.viewControllerType == BalanceSheetViewControllerType_Report)
    {
        addNewEntryViewController = [[AddNewEntryViewController alloc] initWithNibName:@"AddNewEntryViewReport" bundle:nil];
        self.title = @"Add Bank";
    }
    else
    {
        addNewEntryViewController = [[AddNewEntryViewController alloc] initWithNibName:@"AddNewEntryViewIncomeOutcome" bundle:nil];
        self.title = [NSString stringWithFormat:@"Add %@", self.dataSource.viewControllerTypeString];
        id obj = [self.dataSource prepareEntryForEdit];
        if ([obj isKindOfClass:[MoneyEntry class]])
        {
            self.title = [NSString stringWithFormat:@"Edit %@", self.dataSource.viewControllerTypeString];
        }
    }
    addNewEntryViewController.editMode = self.editMode;
    
    addNewEntryViewController.dataSource = self.dataSource;
    
    // Set the loaded view controller as our subview.
    [self.view addSubview:addNewEntryViewController.view];
    
    // Resize the loaded view controller's view frame to match our own.
    addNewEntryViewController.view.frame = CGRectMake(0.0f, 0.0f, self.view.frame.size.width, self.view.frame.size.height);
    
    addNewEntryViewController.view.translatesAutoresizingMaskIntoConstraints = NO;
    
    id view = addNewEntryViewController.view;
    id guide = self.topLayoutGuide;
    id views = NSDictionaryOfVariableBindings(guide,view);
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[guide][view]|" options:0 metrics:nil views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:views]];
    
    
    // Finally, set the new view controller as our child.
    [self addChildViewController:addNewEntryViewController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)handleDonePressed:(id)sender {
    for (id<AddNewEntrySelectionViewControllerDelegate> child in self.childViewControllers)
    {
        if ([child respondsToSelector:@selector(doneButtonClicked:)])
        {
            [child performSelector:@selector(doneButtonClicked:) withObject:sender];
        }
    }
}

- (IBAction)handleCancelPressed:(id)sender {
    for (id<AddNewEntrySelectionViewControllerDelegate> child in self.childViewControllers)
    {
        if ([child respondsToSelector:@selector(cancelButtonClicked:)])
        {
            [child performSelector:@selector(cancelButtonClicked:) withObject:sender];
        }
    }
}
@end
