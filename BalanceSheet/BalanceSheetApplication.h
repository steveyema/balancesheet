//
//  BalanceSheetApplication.h
//  BalanceSheet
//
//  Created by Ye Ma on 2016-06-05.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, BalanceSheetTab) {
    BalanceSheetTab_Income = 0,
    BalanceSheetTab_Outcome,
    BalanceSheetTab_Loan,
    BalanceSheetTab_Report
};

typedef NS_ENUM(NSInteger, BalanceSheetDataSourceType)
{
    BalanceSheetDataSource_Day = 0,
    BalanceSheetDataSource_Week = 1,
    BalanceSheetDataSource_Month = 2,
    BalanceSheetDataSource_Year = 3
};

@interface BalanceSheetApplication : NSObject

@property(nonatomic, assign) BalanceSheetTab activeBalanceSheetTab;
@property(nonatomic, assign) BalanceSheetDataSourceType activeBalanceSheetDataSourceType;
+(instancetype)sharedInstance;

@end
