//
//  ReportSectionHeaderDataSource.m
//  BalanceSheet
//
//  Created by Ye Ma on 2016-08-02.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "ReportSectionHeaderDataSource.h"
#import "ReportSectionHeaderCollectionViewCell.h"

@interface ReportSectionHeaderDataSource()<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property(nonatomic, strong) NSArray *tabList;
@property(nonatomic, weak) id<ReportSectionHeaderDataSourceDelegate> delegate;

@end

@implementation ReportSectionHeaderDataSource

- (id)initWithCollectionView:(UICollectionView *)collectionView andDelegate:(id<ReportSectionHeaderDataSourceDelegate>)delegate
{
    if (self = [super init])
    {
        _tabList = @[@"Day", @"Week", @"Month", @"Year"];
        collectionView.dataSource = self;
        collectionView.delegate = self;
        
        _delegate = delegate;
        
        [collectionView registerClass:[ReportSectionHeaderCollectionViewCell class] forCellWithReuseIdentifier:[ReportSectionHeaderCollectionViewCell reuseIdentifier]];
        
//        collectionView.collectionViewLayout
    }
    return self;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.tabList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ReportSectionHeaderCollectionViewCell *cell = (ReportSectionHeaderCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:[ReportSectionHeaderCollectionViewCell reuseIdentifier] forIndexPath:indexPath];
    cell.titleLabel.text = [self.tabList objectAtIndex:indexPath.row];
    if (indexPath.row == [BalanceSheetApplication sharedInstance].activeBalanceSheetDataSourceType)
    {
        [cell setBackgroundColor:[UIColor orangeColor]];
    }
    else
    {
        [cell setBackgroundColor:[UIColor clearColor]];
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [[BalanceSheetApplication sharedInstance] setActiveBalanceSheetDataSourceType:indexPath.row];
    [self.delegate reportSectionSelected:indexPath];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(100, 60);
}


@end
