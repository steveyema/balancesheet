//
//  ReportSectionHeaderCollectionView.h
//  BalanceSheet
//
//  Created by Ye Ma on 2016-08-02.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ReportSectionHeaderDataSourceDelegate;

@interface ReportSectionHeaderCollectionView : UITableViewHeaderFooterView

+(NSString *)uniqueIdentifier;
- (void)initDataSourceWithDelegate:(id<ReportSectionHeaderDataSourceDelegate>)delegate;

@end
