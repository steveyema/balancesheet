//
//  ReportOperationForBreakdown.m
//  BalanceSheet
//
//  Created by Ye Ma on 2016-07-03.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "ReportOperationForBreakdown.h"
#import "MoneyStateEntry.h"
#import "MoneyEntry+Reoccur.h"
#import "BalanceSheetDBController.h"

@implementation ReportOperationForBreakdown

-(void)main
{
    if (self.cancelled)
    {
        return;
    }
    
    MoneyStateEntry * moneyStateEntry = (MoneyStateEntry *)self.stateEntry;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@" datetime == %@ AND type == 0 AND (disabled == NO || disabled == NULL) AND name == %@", [moneyStateEntry.moneyEntry.datetime dateOnly], moneyStateEntry.moneyEntry.name];
    NSArray *result = [[BalanceSheetDBController sharedInstance] loadEntriesFromBackgroundContextWithPredicate:predicate];
    
    if (self.cancelled)
    {
        return;
    }
    
    if (result && result.count > 0)
    {
        moneyStateEntry.duplicateEntryExists = YES;
        MoneyEntry *moneyEntry = (MoneyEntry *)result[0];
        moneyStateEntry.moneyEntry = moneyEntry;
    }
    
    moneyStateEntry.state = EntryState_Ready;
}


@end
