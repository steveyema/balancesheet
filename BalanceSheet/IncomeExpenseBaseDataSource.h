//
//  IncomeExpenseBaseDataSource.h
//  BalanceSheet
//
//  Created by Ye Ma on 2016-07-15.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "BalanceSheetTabDataSource.h"

typedef NS_ENUM(NSInteger, IncomeExpenseDataSourceType)
{
    IncomeExpenseDataSourceType_Regular = 0,
    IncomeExpenseDataSourceType_Loan
};

@interface IncomeExpenseBaseDataSource : BalanceSheetTabDataSource<UITableViewDelegate, UITableViewDataSource>

+ (id)dataSourceFor:(IncomeExpenseDataSourceType)incomeExpenseDataSourceType WithDelegate:(id<BalanceSheetDataSourceDelegate>)delegate andTableView:(UITableView *)tableView;
- (BOOL)presentBreakdownActionSheetForIndexPath:(NSIndexPath *)indexPath FromViewController:(UIViewController *)viewController withSourceView:(UIView *)view;

@end
