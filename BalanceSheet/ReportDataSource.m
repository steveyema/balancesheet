//
//  BalanceSheetDataSource.m
//  BalanceSheet
//
//  Created by Ye Ma on 2016-06-02.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "ReportDataSource.h"
#import "ReportEntry.h"
#import <UIKit/UIKit.h>
#import "ReportEntryTableViewCell.h"
#import "ReportSectionHeaderView.h"
#import "MoneyEntry+Reoccur.h"
#import "ReportSectionHeaderCollectionView.h"
#import "ReportSectionHeaderDataSource.h"

@interface ReportDataSource()<UITableViewDataSource, UITableViewDelegate, ReportSectionHeaderDataSourceDelegate>

@property(nonatomic, assign) BalanceSheetDataSourceType balanceSheetDataSourceType;
@property(nonatomic, strong) BankAccount *bankAccount;
@property(nonatomic, strong) NSMutableArray *bankAccounts;

@end

@implementation ReportDataSource

+(id)dataSourceWithBalanceSheetDataSourceType:(BalanceSheetDataSourceType)balanceSheetDataSourceType andDelegate:(id<BalanceSheetDataSourceDelegate>)delegate andTableView:(UITableView *)tableView
{
    ReportDataSource *dataSource = [[ReportDataSource alloc] initWithTableView:tableView];
    dataSource.balanceSheetDataSourceType = balanceSheetDataSourceType;
    dataSource.delegate = delegate;
    return dataSource;
}

-(id)initWithTableView:(UITableView *)tableView
{
    if (self = [super init])
    {
        tableView.delegate = self;
        tableView.dataSource = self;
        
        [tableView registerNib:[UINib nibWithNibName:@"ReportEntryTableViewCell" bundle:nil] forCellReuseIdentifier:REPORT_ENTRY_CELL_IDENTIFIER];
        [tableView registerNib:[UINib nibWithNibName:@"ReportLoadingTableViewCell" bundle:nil] forCellReuseIdentifier:REPORT_LOADING_CELL_IDENTIFIER];
        
        [tableView registerClass:[ReportSectionHeaderView class] forHeaderFooterViewReuseIdentifier:[ReportSectionHeaderView uniqueIdentifier]];
        [tableView registerClass:[ReportSectionHeaderCollectionView class] forHeaderFooterViewReuseIdentifier:[ReportSectionHeaderCollectionView uniqueIdentifier]];
        
        [self updateDataScreen];
    }
    return self;
}

- (BankAccount *)bankAccount
{
    if (!_bankAccount)
    {
        _bankAccount = [[BalanceSheetDBController sharedInstance] latestBankAccountRecord];
    }
    return _bankAccount;
}

- (void)loadBankAccountsWithCompletionBlock:(void(^)())completionBlock
{
    NSLog(@"loadBankAccountsWithCompletionBlock");
    [[BalanceSheetDBController sharedInstance] loadBankAccountFromBackgroundContextWithPredicate:nil withCompletionBlock:^(NSArray *results) {
        //
        self.bankAccounts = [results mutableCopy];
        if (completionBlock)
        {
            completionBlock();
        }
    }];
}

#pragma mark - UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 60;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *sectionHeaderView;
    
    if (section == 0)
    {
        ReportSectionHeaderView *sectionHeader = (ReportSectionHeaderView *)[tableView dequeueReusableHeaderFooterViewWithIdentifier:[ReportSectionHeaderView uniqueIdentifier]];
        sectionHeader.headerLabel.text = @"Bank Record";
        sectionHeaderView = sectionHeader;
    }
    else
    {
        ReportSectionHeaderCollectionView *sectionHeader = (ReportSectionHeaderCollectionView *)[tableView dequeueReusableHeaderFooterViewWithIdentifier:[ReportSectionHeaderCollectionView uniqueIdentifier]];
//        sectionHeader.headerLabel.text = @"Balance sheet";
        [sectionHeader initDataSourceWithDelegate:self];
        sectionHeaderView = sectionHeader;
    }
    return sectionHeaderView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
    {
        return self.bankAccounts.count;
    }
    else
    {
        return PageSize * self.page + 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        ReportEntryTableViewCell *cell = (ReportEntryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:REPORT_ENTRY_CELL_IDENTIFIER];

        cell.backgroundColor = [UIColor clearBackgroundColor];
        
        BankAccount *account = (BankAccount *)self.bankAccounts[indexPath.row];
        cell.date.text = [NSString stringWithFormat:@"%@", account.datetime.shortDate];
        cell.balance.text = [NSString stringWithFormat:@"%@", [[account.balance unarchive] standardFormat]];
        return cell;
    }
    else
    {
        if (indexPath.row == PageSize *self.page)
        {
            //end of the current list
            ReportEntryTableViewCell *cell = (ReportEntryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:REPORT_LOADING_CELL_IDENTIFIER];
            cell.completeBlock = ^{
                self.page ++;
                [tableView reloadData];
            };
            return cell;
        }
        else
        {
            ReportEntryTableViewCell *cell = (ReportEntryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:REPORT_ENTRY_CELL_IDENTIFIER];
            
            NSDate *startDate = self.bankAccount.datetime? self.bankAccount.datetime : [NSDate date];
            NSDate *endDate = [self endDateFromStartDate:startDate forIndexPath:indexPath];
            
            ReportEntry *reportEntry = self.reportEntries[indexPath];
            if (!reportEntry)
            {
                reportEntry = [ReportEntry entryWithStartDate:startDate andEndDate:endDate];
                [self.reportEntries setObject:reportEntry forKey:indexPath];
            }
            
            //set amount;
            cell.date.text = [NSString stringWithFormat:@"%@", [endDate shortDate]];
            //        cell.balance.text = [NSString stringWithFormat:@"%ld", [self.bankAccount.balance integerValue] - reportEntry.amount];
            double amount = [[self.bankAccount.balance unarchive] doubleValue] + [[reportEntry.amount unarchive] doubleValue];
            
            if (amount < BANK_ACCOUNT_LIMIT)
            {
                cell.backgroundColor = [UIColor highlightedCellBackgroundColor];
            }
            else
            {
                cell.backgroundColor = [UIColor clearBackgroundColor];
            }
            NSString *formattedAmount = [@(amount) standardFormat];
            cell.balance.text = [NSString stringWithFormat:@"%@", formattedAmount];
            
            if (reportEntry.state == EntryState_New)
            {
                [self startOperationForReportEntry:reportEntry atIndexPath:indexPath];
            }
            // set older date background color to gray
            if ([[endDate dateOnly] compare:[[NSDate date] dateOnly]] == NSOrderedAscending)
            {
                cell.backgroundColor = [UIColor disabledCellBackgroundColor];
            }
            
            return cell;
        }
    }
}

- (NSDate *)endDateFromStartDate:(NSDate *)startDate forIndexPath:(NSIndexPath *)indexPath
{
    if (self.balanceSheetDataSourceType == BalanceSheetDataSource_Day)
    {
        return [startDate dateByAddingTimeInterval:60*60*24*indexPath.row];
    }
    else if (self.balanceSheetDataSourceType == BalanceSheetDataSource_Week)
    {
        return [startDate adjustByNumberOfWeek:indexPath.row keepOriginalDate:indexPath.row == 0];
//        return [startDate dateByAddingTimeInterval:7*60*60*24*indexPath.row];
    }
    else if (self.balanceSheetDataSourceType == BalanceSheetDataSource_Month)
    {
        return [startDate adjustByNumberOfMonth:indexPath.row keepOriginalDate:indexPath.row == 0];
    }
    else
    {
        return [startDate adjustByNumberOfYear:indexPath.row keepOriginalDate:indexPath.row == 0];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return indexPath.section == 0;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //add code here for when you hit delete
        NSLog(@"deleting");

        id objToRemove = self.bankAccounts[indexPath.row];
        if ([[BalanceSheetDBController sharedInstance] deleteManagedObject:objToRemove])
        {
            [self.bankAccounts removeObject:objToRemove];

        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.delegate respondsToSelector:@selector(tableView:didSelectRowAtIndexPath:)])
    {
        [self.delegate tableView:tableView didSelectRowAtIndexPath:indexPath];
    }
}

#pragma mark - Abstract
- (ReportOperationType)reportOperationType
{
    return ReportOperationType_Detail;
}

- (void)updateDataScreen
{
    [self resetReportEntries];
    self.bankAccount = nil;
    
    //refresh listOfEntries;
    [self loadBankAccountsWithCompletionBlock:^{
        //refresh UI
        if ([self.delegate respondsToSelector:@selector(updateUI)])
        {
            [self.delegate updateUI];
        }
    }];
}

- (void)resetReportEntries
{
    [self.reportEntries removeAllObjects];
}

- (void)addNewEntry:(NSDictionary *)entryDic
{
    NSDictionary *bankAccount = entryDic;
    BankAccount *entry = (BankAccount *)[NSEntityDescription insertNewObjectForEntityForName:BANKACCOUNT
                                                                      inManagedObjectContext:[BalanceSheetDBController sharedInstance].mainManagedObjectContext];
    entry.name = [bankAccount objectForKey:MONEYENTRY_NAME];
    entry.datetime = [bankAccount objectForKey:MONEYENTRY_DATETIME];
    entry.balance = [bankAccount objectForKey:MONEYENTRY_AMOUNT];
    
    NSError *error;
    if (![[BalanceSheetDBController sharedInstance].mainManagedObjectContext save:&error]) {
        NSLog(@"Failed to save - error: %@", [error localizedDescription]);
    }
}

#pragma mark - ReportSectionHeaderDataSourceDelegate
- (void)reportSectionSelected:(NSIndexPath *)indexPath
{
    self.balanceSheetDataSourceType = indexPath.row;
    [self updateDataScreen];
}


@end
