//
//  ReportOperationWithDetail.m
//  BalanceSheet
//
//  Created by Ye Ma on 2016-06-10.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "ReportOperationWithTotalAmount.h"
#import "ReportEntry.h"
#import "MoneyEntry+Reoccur.h"

@implementation ReportOperationWithTotalAmount

-(void)main
{
    [super main];
    
    __block NSInteger totalAmount = 0;
    
    ReportEntry *reportEntry = (ReportEntry *)self.stateEntry;
    
    [reportEntry.detailInstances enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        MoneyEntry *moneyEntry = (MoneyEntry *)obj;
        totalAmount += [moneyEntry.amount integerValue];
    }];
    reportEntry.amount = @(totalAmount);
}

@end
