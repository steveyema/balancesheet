//
//  ReportEntry.h
//  BalanceSheet
//
//  Created by Ye Ma on 2016-06-02.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StateEntry.h"

@interface ReportEntry : StateEntry

@property(nonatomic, strong) NSDate *startDate;
@property(nonatomic, strong) NSDate *endDate;
@property(nonatomic, strong) NSNumber *amount;
@property(nonatomic, strong) NSArray *detailInstances;
@property(nonatomic, strong) NSArray *sortedDetailInstances;

+(id)entryWithStartDate:(NSDate *)startDate andEndDate:(NSDate *)endDate;

@end
