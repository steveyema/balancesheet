//
//  AddNewEntrySelectionViewController.h
//  BalanceSheet
//
//  Created by Ye Ma on 2016-06-03.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AddNewEntrySelectionViewControllerDelegate <NSObject>

- (void)doneButtonClicked:(id)sender;
- (void)cancelButtonClicked:(id)sender;

@end



@class BalanceSheetTabDataSource;

@interface AddNewEntrySelectionViewController : UIViewController

@property(nonatomic, strong) BalanceSheetTabDataSource *dataSource;
@property(nonatomic, assign) BOOL editMode;

@end
