//
//  BalanceSheetDBController.m
//  BalanceSheet
//
//  Created by Ye Ma on 2016-05-31.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "BalanceSheetDBController.h"

@implementation BalanceSheetDBController

@synthesize mainManagedObjectModel = _mainManagedObjectModel, mainManagedObjectContext = _mainManagedObjectContext, backgroundManagedObjectContext = _backgroundManagedObjectContext, mainPersistentStoreCoordinator = _mainPersistentStoreCoordinator;

+(instancetype)sharedInstance
{
    static dispatch_once_t onceToken;
    static BalanceSheetDBController *controller = nil;
    dispatch_once(&onceToken, ^{
        controller = [[BalanceSheetDBController alloc] init];
    });
    return controller;
}

- (id)init
{
    if (self = [super init])
    {
        [self registerForSaveNotifications];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (NSManagedObjectContext *)mainManagedObjectContext
{
    if (!_mainManagedObjectContext)
    {
        _mainManagedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        [_mainManagedObjectContext setParentContext:[self backgroundManagedObjectContext]];
    }
    return _mainManagedObjectContext;
}

- (NSManagedObjectContext *)backgroundManagedObjectContext
{
    if (_backgroundManagedObjectContext != nil)
    {
        return _backgroundManagedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self mainPersistentStoreCoordinator];
    
    if (coordinator != nil)
    {
        _backgroundManagedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        [_backgroundManagedObjectContext setPersistentStoreCoordinator:coordinator];
        [_backgroundManagedObjectContext setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];
        // iOS defaults to nil anyways
        [_backgroundManagedObjectContext setUndoManager:nil];
    }
    
    return _backgroundManagedObjectContext;
}


- (NSManagedObjectModel *)mainManagedObjectModel
{
    if (!_mainManagedObjectModel)
    {
        _mainManagedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    }
    return _mainManagedObjectModel;
}

- (NSPersistentStoreCoordinator *)mainPersistentStoreCoordinator
{
    if (!_mainPersistentStoreCoordinator)
    {
        NSString *storePath = pathInAppDirectory(NSApplicationSupportDirectory, BALANCESHEETDBFILE, YES);
        NSURL *storeUrl = [NSURL fileURLWithPath:storePath];

        _mainPersistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.mainManagedObjectModel];

        NSError *error;
        if (![_mainPersistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeUrl options:@{NSMigratePersistentStoresAutomaticallyOption : @(YES), NSInferMappingModelAutomaticallyOption : @(YES)} error:&error])
        {
            NSLog(@"Could not create a new balancesheet store, terminate app. Error: %@", error.localizedDescription);
            abort();
        }
    }
    
    return _mainPersistentStoreCoordinator;
}

- (void)registerForSaveNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(mainContextDidSave:) name:NSManagedObjectContextDidSaveNotification object:self.mainManagedObjectContext];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(mainContextDidChange:) name:NSManagedObjectContextObjectsDidChangeNotification object:self.mainManagedObjectContext];
}

- (void)mainContextDidChange:(NSNotification *)notification
{
    [self.mainManagedObjectContext performBlock:^{
        [self.mainManagedObjectContext save:nil];
    }];
}

- (void)mainContextDidSave:(NSNotification *)notification
{
    // When we save the main context, save the background context.
    // This triggers a save to the persistent store.
    [self.backgroundManagedObjectContext performBlock:^{
        NSError *err = nil;
        [self.backgroundManagedObjectContext save:&err];
        
        if (err)
        {
            // Our failure to save the managed object context is likely because of a full file system.  Inform the user of this.
            // We could probably check the different error codes here in the future.
            dispatch_async(dispatch_get_main_queue(), ^{
//                DDLogError(@"Error saving to background context to disk -- %@", err);
//                [[AppCore sharedInstance] handleContextSaveError];
            });
        }
    }];
}


#pragma mark - SDK
- (BankAccount *)latestBankAccountRecord
{
    BankAccount *bankAccount = nil;
    
    NSManagedObjectContext *moc = self.mainManagedObjectContext;
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:BANKACCOUNT];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"datetime" ascending:NO]];
    
    NSError *error = nil;
    NSArray *results = [moc executeFetchRequest:request error:&error];
    if (results)
    {
        if (results.count > 0)
        {
            bankAccount = (BankAccount *)results.firstObject;
        }
    }

    return bankAccount;
}

- (void)addNewBankAccount:(NSDictionary *)bankAccount
{
    BankAccount *entry = (BankAccount *)[NSEntityDescription insertNewObjectForEntityForName:BANKACCOUNT
                                                                      inManagedObjectContext:[BalanceSheetDBController sharedInstance].mainManagedObjectContext];
    entry.name = [bankAccount objectForKey:MONEYENTRY_NAME];
    entry.datetime = [bankAccount objectForKey:MONEYENTRY_DATETIME];
    entry.balance = [bankAccount objectForKey:MONEYENTRY_AMOUNT];
    
    NSError *error;
    if (![[BalanceSheetDBController sharedInstance].mainManagedObjectContext save:&error]) {
        NSLog(@"Failed to save - error: %@", [error localizedDescription]);
    }
}

- (void)loadBankAccountFromBackgroundContextWithPredicate:(NSPredicate *)predicate withCompletionBlock:(void(^)(NSArray *))completionBlock
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSArray *results = nil;
        
        NSManagedObjectContext *moc = self.backgroundManagedObjectContext;
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:BANKACCOUNT];
        if (predicate) request.predicate = predicate;
        request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"datetime" ascending:NO]];
        
        NSError *error = nil;
        results = [moc executeFetchRequest:request error:&error];
        if (!results) {
            NSLog(@"Error fetching Bank Account objects: %@\n%@", [error localizedDescription], [error userInfo]);
            abort();
        }
        
        if (completionBlock)
        {
            completionBlock(results);
        }
    });
}


- (BOOL)deleteManagedObject:(NSManagedObject *)obj
{
    [self.backgroundManagedObjectContext deleteObject:obj];
    NSError *error;
    return [self.backgroundManagedObjectContext save:&error];
    
}

- (NSArray *)loadEntriesFromMainContextWithPredicate:(NSPredicate *)predicate
{
    NSArray *results = nil;
    
    NSManagedObjectContext *moc = self.mainManagedObjectContext;
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:MONEYENTRY];
    if (predicate) request.predicate = predicate;
    
    NSError *error = nil;
    results = [moc executeFetchRequest:request error:&error];
    if (!results) {
        NSLog(@"Error fetching Employee objects: %@\n%@", [error localizedDescription], [error userInfo]);
        abort();
    }
    
    return results;
}

- (NSArray *)loadEntriesFromBackgroundContextWithPredicate:(NSPredicate *)predicate
{
    NSArray *results = nil;
    
    NSManagedObjectContext *moc = self.backgroundManagedObjectContext;
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:MONEYENTRY];
    if (predicate) request.predicate = predicate;
    
    NSError *error = nil;
    results = [moc executeFetchRequest:request error:&error];
    if (!results) {
        NSLog(@"Error fetching Employee objects: %@\n%@", [error localizedDescription], [error userInfo]);
        abort();
    }
    
    return results;
}


- (void)loadEntriesFromBackgroundContextWithPredicate:(NSPredicate *)predicate withCompletionBlock:(void(^)(NSArray *))completionBlock
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSArray *results = nil;
        
        NSManagedObjectContext *moc = self.backgroundManagedObjectContext;
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:MONEYENTRY];
        if (predicate) request.predicate = predicate;
        NSSortDescriptor *descriptorName = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
        NSSortDescriptor *descriptorDatetime = [NSSortDescriptor sortDescriptorWithKey:@"vdatetime" ascending:NO];
        [request setSortDescriptors:@[descriptorName, descriptorDatetime]];
        
        NSError *error = nil;
        results = [moc executeFetchRequest:request error:&error];
        if (!results) {
            NSLog(@"Error fetching Money Entry objects: %@\n%@", [error localizedDescription], [error userInfo]);
            abort();
        }
        
        if (completionBlock)
        {
            completionBlock(results);
        }
    });
}




@end
