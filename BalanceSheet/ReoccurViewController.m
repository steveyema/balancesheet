//
//  ReoccurViewController.m
//  BalanceSheet
//
//  Created by Ye Ma on 2016-06-03.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "ReoccurViewController.h"
#import "ReoccurTypeTableViewCell.h"

@interface ReoccurViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSArray *titleArray;

@end

@implementation ReoccurViewController

+ (id)viewControllerWithDelegate:(id<ReoccurViewControllerDeleate>)delegate
{
    ReoccurViewController *reoccurViewController = [[ReoccurViewController alloc] initWithNibName:@"ReoccurViewController" bundle:nil];
    reoccurViewController.delegate = delegate;
    return reoccurViewController;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    self.titleArray = [self.delegate reoccurTitleArray];
    
    [self.tableView registerClass:[ReoccurTypeTableViewCell class] forCellReuseIdentifier:[ReoccurTypeTableViewCell uniqueIdentifier]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return BalanceSheetEntryType_Reoccur_Count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ReoccurTypeTableViewCell *cell = (ReoccurTypeTableViewCell *)[tableView dequeueReusableCellWithIdentifier:[ReoccurTypeTableViewCell uniqueIdentifier]];
    cell.titleLabel.text = self.titleArray[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.delegate respondsToSelector:@selector(doneSelectReoccurType:withDisplayText:)])
    {
        [self.delegate doneSelectReoccurType:[indexPath row] withDisplayText:self.titleArray[indexPath.row]];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
