//
//  BalanceSheetDataSource.h
//  BalanceSheet
//
//  Created by Ye Ma on 2016-05-31.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "IncomeExpenseBaseDataSource.h"

@interface IncomeExpenseDataSource : IncomeExpenseBaseDataSource

@end
