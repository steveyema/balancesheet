//
//  BalanceSheetApplication.m
//  BalanceSheet
//
//  Created by Ye Ma on 2016-06-05.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "BalanceSheetApplication.h"

@interface BalanceSheetApplication()
{
    BalanceSheetTab _activeBalanceSheetTab;
    BalanceSheetDataSourceType _activeBalanceSheetDataSourceType;
}

@end

@implementation BalanceSheetApplication

+(instancetype)sharedInstance
{
    static BalanceSheetApplication *application = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        //
        application = [[BalanceSheetApplication alloc] init];
    });
    return application;
}

-(id)init
{
    if (self = [super init])
    {
        _activeBalanceSheetTab = [[[NSUserDefaults standardUserDefaults] objectForKey:ACTIVE_BALANCE_SHEET_TAB_KEY] integerValue];
        _activeBalanceSheetDataSourceType = [[[NSUserDefaults standardUserDefaults] objectForKey:ACTIVE_BALANCE_SHEET_DATA_SOURCE_TYPE_KEY] integerValue];
    }
    return self;
}

-(void)setActiveBalanceSheetTab:(BalanceSheetTab)activeBalanceSheetTab
{
    _activeBalanceSheetTab = activeBalanceSheetTab;
    [[NSUserDefaults standardUserDefaults] setObject:@(activeBalanceSheetTab) forKey:ACTIVE_BALANCE_SHEET_TAB_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void)setActiveBalanceSheetDataSourceType:(BalanceSheetDataSourceType)activeBalanceSheetDataSourceType
{
    _activeBalanceSheetDataSourceType = activeBalanceSheetDataSourceType;
    [[NSUserDefaults standardUserDefaults] setObject:@(activeBalanceSheetDataSourceType) forKey:ACTIVE_BALANCE_SHEET_DATA_SOURCE_TYPE_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
