//
//  ReoccurTypeTableViewCell.h
//  BalanceSheet
//
//  Created by Ye Ma on 2016-06-03.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReoccurTypeTableViewCell : UITableViewCell

@property (nonatomic, strong) UILabel *titleLabel;
+ (NSString *)uniqueIdentifier;

@end
