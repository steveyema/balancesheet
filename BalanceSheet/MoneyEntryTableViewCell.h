//
//  MoneyEntryTableViewCell.h
//  BalanceSheet
//
//  Created by Ye Ma on 2016-05-31.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MoneyEntryTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@end
