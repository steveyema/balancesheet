//
//  BalanceSheetBreakDownDataSource.m
//  BalanceSheet
//
//  Created by Ye Ma on 2016-06-10.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "BalanceSheetBreakDownDataSource.h"
#import "MoneyEntryTableViewCell.h"
#import "ReportEntry.h"
#import "ReportOperation.h"
#import "ReportOperations.h"
#import "TotalTableViewCell.h"
#import "ReportSectionHeaderView.h"
#import "ReportEntryTableViewCell.h"

@interface BalanceSheetBreakDownDataSource()

@property(nonatomic, strong) ReportOperations *balanceSheetOperations;
@property(nonatomic, strong) ReportEntry *reportEntry;
@property(nonatomic, strong) void(^completionBlock)(void);
@property(nonatomic, strong) NSMutableArray *bankAccounts;

@end

@implementation BalanceSheetBreakDownDataSource

+ (id)dataSourceWithTableView:(UITableView *)tableView andDelegate:(id<BalanceSheetDataSourceDelegate>)delegate andCompletionBlock:(void(^)(void))completionBlock
{
    BalanceSheetBreakDownDataSource *dataSource = [[BalanceSheetBreakDownDataSource alloc] initWithTableView:tableView];
    dataSource.delegate = delegate;
    dataSource.completionBlock = completionBlock;
    return dataSource;
}

- (id)initWithTableView:(UITableView *)tableView
{
    if (self = [super init])
    {
        _balanceSheetOperations = [[ReportOperations alloc] init];
        tableView.dataSource = self;
        tableView.delegate = self;
    
        [tableView registerNib:[UINib nibWithNibName:@"MoneyEntry" bundle:nil] forCellReuseIdentifier:@"MoneyEntryCell"];
        [tableView registerNib:[UINib nibWithNibName:@"TotalCell" bundle:nil] forCellReuseIdentifier:@"TotalCell"];
        [tableView registerClass:[ReportSectionHeaderView class] forHeaderFooterViewReuseIdentifier:[ReportSectionHeaderView uniqueIdentifier]];
        [tableView registerNib:[UINib nibWithNibName:@"ReportEntryTableViewCell" bundle:nil] forCellReuseIdentifier:REPORT_ENTRY_CELL_IDENTIFIER];
        
        [self updateDataScreen];
    }
    return self;
}

- (void)viewDidLoad
{
    if ([self.delegate respondsToSelector:@selector(selectedItem)])
    {
        self.reportEntry = (ReportEntry *)[self.delegate selectedItem];
        [self startOperationForReportEntry];
    }
}

-(void)startOperationForReportEntry
{
    if (!self.reportEntry) return;
    
    ReportOperation *operation = [ReportOperation operationWithType:ReportOperationType_Basic andStateEntry:self.reportEntry];
    __weak ReportOperation *weakOperation = operation;
    operation.completionBlock = ^{
        
        if (weakOperation.isCancelled)
        {
            return;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (self.completionBlock)
            {
                self.completionBlock();
            }
        });
    };
    [self.balanceSheetOperations.queue addOperation:operation];
}

- (NSNumber *)totalAmount
{
    __block NSInteger total = 0;
    [self.reportEntry.detailInstances enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        MoneyEntry *entry = (MoneyEntry *)obj;
        total += [entry.amount integerValue];
    }];
    return [@(total) unarchive];
}

- (void)loadBankAccountsWithCompletionBlock:(void(^)())completion
{
    NSLog(@"loadBankAccountsWithCompletionBlock");
    [[BalanceSheetDBController sharedInstance] loadBankAccountFromBackgroundContextWithPredicate:nil withCompletionBlock:^(NSArray *results) {
        //
        self.bankAccounts = [results mutableCopy];
        if (completion)
        {
            completion();
        }
    }];
}

#pragma mark - Abstract
- (void)updateDataScreen
{
    //refresh listOfEntries;
    [self loadBankAccountsWithCompletionBlock:^{
        //refresh UI
        dispatch_async(dispatch_get_main_queue(), ^{
            if (self.completionBlock)
            {
                self.completionBlock();
            }
        });
    }];
}

#pragma mark - UITableViewDataSource UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 60;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    ReportSectionHeaderView *sectionHeader = (ReportSectionHeaderView *)[tableView dequeueReusableHeaderFooterViewWithIdentifier:[ReportSectionHeaderView uniqueIdentifier]];
    if (section == 0)
    {
        sectionHeader.headerLabel.text = @"Bank Record";
    }
    else
    {
        sectionHeader.headerLabel.text = @"Balance Sheet Breakdown";
    }
    return sectionHeader;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
    {
        return self.bankAccounts.count;
    }
    else
    {
        return self.reportEntry.sortedDetailInstances? (self.reportEntry.sortedDetailInstances.count + 1) : 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        ReportEntryTableViewCell *cell = (ReportEntryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:REPORT_ENTRY_CELL_IDENTIFIER];
        
        cell.backgroundColor = [UIColor clearBackgroundColor];
        
        BankAccount *account = (BankAccount *)self.bankAccounts[indexPath.row];
        cell.date.text = [NSString stringWithFormat:@"%@", account.datetime.shortDate];
        cell.balance.text = [NSString stringWithFormat:@"%@", [[account.balance unarchive] standardFormat]];
        return cell;
    }
    else
    {
        if (indexPath.row == self.reportEntry.sortedDetailInstances.count)
        {
            TotalTableViewCell *cell = (TotalTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"TotalCell"];
            cell.totalLabel.text = [NSString stringWithFormat:@"%@", [[self totalAmount] standardFormat]];
            return cell;
        }
        else
        {
            MoneyEntry *moneyEntry = (MoneyEntry *)self.reportEntry.sortedDetailInstances[indexPath.row];
            
            MoneyEntryTableViewCell *cell = (MoneyEntryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"MoneyEntryCell"];
            cell.nameLabel.text = moneyEntry.name;
            cell.amountLabel.text = [NSString stringWithFormat:@"%@", [[moneyEntry.amount unarchive] standardFormat]];
            cell.dateLabel.text = [NSString stringWithFormat:@"%@", moneyEntry.vdatetime.shortDate];
            return cell;
        }
    }
}

@end
