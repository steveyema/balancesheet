//
//  ReportDataSourceBase.h
//  BalanceSheet
//
//  Created by Ye Ma on 2016-07-03.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BalanceSheetTabDataSource.h"
#import "ReportOperation.h"

@class ReportEntry;

@interface ReportDataSourceBase : BalanceSheetTabDataSource

@property(nonatomic, assign) NSInteger page;
@property(nonatomic, strong) NSMutableDictionary *reportEntries;
@property(nonatomic, assign) ReportOperationType reportOperationType;

-(void)startOperationForReportEntry:(StateEntry *)stateEntry atIndexPath:(NSIndexPath *)indexPath;
- (void)resetReportEntries;

@end
