//
//  BalanceSheetDataSource.h
//  BalanceSheet
//
//  Created by Ye Ma on 2016-06-05.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MainTabBarController.h"

@interface BalanceSheetDataSource : NSObject

@property(nonatomic, weak) id<BalanceSheetDataSourceDelegate> delegate;

- (void)viewDidLoad;
- (void)viewWillAppear:(BOOL)animated;
- (void) updateDataScreen;
- (void)preEntryCreation:(UIViewController *)viewController;
- (id)prepareEntryForEdit;
- (void)postEntryForEdit;
- (void)addNewEntry:(NSDictionary *)entryDic;
- (void)editExistingEntry:(NSDictionary *)entry;

@end
