//
//  BankAccount.h
//  BalanceSheet
//
//  Created by Ye Ma on 2016-06-02.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface BankAccount : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "BankAccount+CoreDataProperties.h"
