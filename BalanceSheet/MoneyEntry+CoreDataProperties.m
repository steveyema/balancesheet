//
//  MoneyEntry+CoreDataProperties.m
//  BalanceSheet
//
//  Created by Ye Ma on 2016-05-31.
//  Copyright © 2016 Ye Ma. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "MoneyEntry+CoreDataProperties.h"

@implementation MoneyEntry (CoreDataProperties)

@dynamic name;
@dynamic datetime;
@dynamic vdatetime;
@dynamic amount;
@dynamic type;
@dynamic disabled;

@end
