//
//  BalanceSheetDBController.h
//  BalanceSheet
//
//  Created by Ye Ma on 2016-05-31.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface BalanceSheetDBController : NSObject

@property(nonatomic, strong, readonly) NSManagedObjectContext *mainManagedObjectContext;
@property(nonatomic, strong, readonly) NSManagedObjectContext *backgroundManagedObjectContext;
@property(nonatomic, strong, readonly) NSManagedObjectModel *mainManagedObjectModel;
@property(nonatomic, strong, readonly) NSPersistentStoreCoordinator *mainPersistentStoreCoordinator;

+(instancetype)sharedInstance;
- (BankAccount *)latestBankAccountRecord;
- (void)loadBankAccountFromBackgroundContextWithPredicate:(NSPredicate *)predicate withCompletionBlock:(void(^)(NSArray *))completionBlock;
- (void)addNewBankAccount:(NSDictionary *)bankAccount;
- (NSArray *)loadEntriesFromMainContextWithPredicate:(NSPredicate *)predicate;
- (NSArray *)loadEntriesFromBackgroundContextWithPredicate:(NSPredicate *)predicate;
- (void)loadEntriesFromBackgroundContextWithPredicate:(NSPredicate *)predicate withCompletionBlock:(void(^)(NSArray *))completionBlock;
- (BOOL)deleteManagedObject:(NSManagedObject *)obj;

@end
