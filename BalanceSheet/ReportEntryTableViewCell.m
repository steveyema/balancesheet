//
//  ReportEntryTableViewCell.m
//  BalanceSheet
//
//  Created by Ye Ma on 2016-06-02.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "ReportEntryTableViewCell.h"

@implementation ReportEntryTableViewCell

//- (void)awakeFromNib {
//    [super awakeFromNib];
//    // Initialization code
//    if (self.completeBlock)
//    {
//        self.completeBlock();
//    }
//}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if (self.completeBlock)
    {
        self.completeBlock();
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
