//
//  UIAlertController+Message.h
//  BalanceSheet
//
//  Created by Ye Ma on 2016-07-10.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertController (Message)

+(void)messageWithTitle:(NSString *)title andMessage:(NSString *)message fromViewController:(UIViewController *)viewController;

@end
