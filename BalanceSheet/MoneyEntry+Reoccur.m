//
//  MoneyEntry+Reoccur.m
//  BalanceSheet
//
//  Created by Ye Ma on 2016-06-03.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "MoneyEntry+Reoccur.h"
#import "NSDate+Formatter.h"
#import "MoneyStateEntry.h"

@implementation MoneyEntry (Reoccur)

- (NSNumber *)reoccurAmountFromStartDate:(NSDate *)startDate tillEndDate:(NSDate *)endDate
{
    NSInteger totalAmount = 0;
    if ([startDate isOlderThanDate:endDate] || [startDate isSameAsDate:endDate])
    {
        NSDate *currentDate = [self.datetime copy];
        
        NSInteger index = 0;
        while ([currentDate isOlderThanDate:startDate])
        {
            currentDate = [self incrementDate:currentDate withIndex:index];
            index++;
        }
        
        while ([currentDate isOlderThanDate:endDate] || [currentDate isSameAsDate:endDate])
        {
            totalAmount += [self.amount integerValue];
            currentDate = [self incrementDate:currentDate withIndex:index];
            index++;
        }
    }
    
    return @(totalAmount);
}

- (NSArray *)reoccurInstanceFromStartDate:(NSDate *)startDate tillEndDate:(NSDate *)endDate
{
    NSMutableArray *results = [[NSMutableArray alloc] init];
    if ([startDate isOlderThanDate:endDate] || [startDate isSameAsDate:endDate])
    {
        NSDate *currentDate = [self.datetime copy];
        
        NSInteger index = 0;
        while ([currentDate isOlderThanDate:startDate])
        {
            currentDate = [self incrementDate:currentDate withIndex:index];
            index++;
        }
        
        while ([currentDate isOlderThanDate:endDate] || [currentDate isSameAsDate:endDate])
        {
            MoneyEntry *copy = [self copy];
            copy.vdatetime = currentDate;
            copy.datetime = currentDate;
            [results addObject:copy];
            currentDate = [self incrementDate:currentDate withIndex:index];
            index++;
        }
    }
    
    return results;
}

- (NSDictionary *)reoccurInstanceFromIndex:(NSIndexPath *)indexPath1 toIndexPath:(NSIndexPath *)indexPath2 baseOn:(NSDate *)date
{
    NSMutableDictionary *results = [[NSMutableDictionary alloc] init];
    
    NSDate *currentDate = [self.datetime copy];
    
    NSInteger baseIndex = 0;
    while ([currentDate isOlderThanDate:date])
    {
        currentDate = [self incrementDate:currentDate withIndex:baseIndex];
        baseIndex++;
    }
    
    NSInteger index = 0;
    while (index < (indexPath2.row - indexPath1.row))
    {
        MoneyStateEntry *stateEntry = [[MoneyStateEntry alloc] init];
        MoneyEntry *copy = [self copy];
        copy.type = BalanceSheetEntryType_OneTime;
        copy.vdatetime = currentDate;
        copy.datetime = currentDate;
        stateEntry.moneyEntry = copy;
        [results setObject:stateEntry forKey:[NSIndexPath indexPathForRow:indexPath1.row + index inSection:0]];
        currentDate = [self incrementDate:currentDate withIndex:index + baseIndex];
        index++;
    }
    
    return results;
}

- (id)copyWithZone:(NSZone *)zone
{
    NSEntityDescription *entity = [NSEntityDescription entityForName:MONEYENTRY inManagedObjectContext:[BalanceSheetDBController sharedInstance].mainManagedObjectContext];
    
    MoneyEntry *entry = [[MoneyEntry alloc] initWithEntity:entity insertIntoManagedObjectContext:nil];
    
    entry.name = [self.name copy];
    entry.vdatetime = [self.vdatetime copy];
    entry.datetime = [self.datetime copy];
    entry.amount = [self.amount copy];
    entry.type = [self.type copy];
    entry.disabled = [self.disabled copy];
    return entry;
}

- (NSDate *)incrementDate:(NSDate *)date withIndex:(NSInteger)index
{
    if ([self.type integerValue] == BalanceSheetEntryType_Reoccur)
    {
        return [self incrementDate:date byNumberOfDays:1];
    }
    else if ([self.type integerValue] == BalanceSheetEntryType_Reoccur_Week)
    {
        return [self incrementDate:date byNumberOfDays:7];
    }
    else if ([self.type integerValue] == BalanceSheetEntryType_Reoccur_Two_Weeks)
    {
        return [self incrementDate:date byNumberOfDays:14];
    }
    else if ([self.type integerValue] == BalanceSheetEntryType_Reoccur_Half_Month)
    {
        if (index % 2 == 0)
        {
            return [self incrementDate:date byNumberOfDays:15];
        }
        else
        {
            date = [self decrementDate:date byNumberOfDays:15];
            return [self incrementDate:date byNumberOfMonths:1];
        }
    }
    else if ([self.type integerValue] == BalanceSheetEntryType_Reoccur_Month)
    {
        return [self incrementDate:date byNumberOfMonths:1];
    }
    else
    {
        return [self incrementDate:date byNumberOfMonths:2];
    }
}

- (NSDate *)incrementDate:(NSDate *)date byNumberOfDays:(NSInteger)days
{
    return [date dateByAddingTimeInterval:60*60*24*days];
}

- (NSDate *)decrementDate:(NSDate *)date byNumberOfDays:(NSInteger)days
{
    return [date dateByAddingTimeInterval:-60*60*24*days];
}

- (NSDate *)incrementDate:(NSDate *)date byNumberOfMonths:(NSInteger)months
{
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setMonth:months];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *newDate = [calendar dateByAddingComponents:dateComponents toDate:date options:0];
    return newDate;
}

@end
