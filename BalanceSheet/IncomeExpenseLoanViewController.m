//
//  IncomeExpenseLoanViewController.m
//  BalanceSheet
//
//  Created by Ye Ma on 2016-07-15.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "IncomeExpenseLoanViewController.h"
#import "IncomeExpenseLoanDataSource.h"

@interface IncomeExpenseLoanViewController()<BalanceSheetDataSourceDelegate>

@end

@implementation IncomeExpenseLoanViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"Loan";
    
    _dataSource = [IncomeExpenseBaseDataSource dataSourceFor:IncomeExpenseDataSourceType_Loan WithDelegate:self andTableView:self.tableView];

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.dataSource viewWillAppear:animated];
}

#pragma mark - BalanceSheetDataSourceDelegate

- (void)updateUI
{
    dispatch_async(dispatch_get_main_queue(), ^{
        //reload data
        NSLog(@"refresh IncomeExpenseViewController");
        [self.tableView reloadData];
    });
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    AddNewEntrySelectionViewController *addNewEntrySelectionViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"AddNewEntrySelectionViewController"];
//    addNewEntrySelectionViewController.dataSource = self.dataSource;
//    addNewEntrySelectionViewController.editMode = YES;
//    
//    UIView *view = [self.tableView cellForRowAtIndexPath:indexPath];
//    [UINavigationController presentPopoverWithRootViewController:addNewEntrySelectionViewController fromViewController:self withSourceView:view withSize:CGSizeMake(400, 550)];
    
}

@end
