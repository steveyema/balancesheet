//
//  IncomeExpenseBreakDownViewController.m
//  BalanceSheet
//
//  Created by Ye Ma on 2016-06-12.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "IncomeExpenseBreakDownViewController.h"
#import "IncomeExpenseBreakdownDataSource.h"
#import "AddNewEntrySelectionViewController.h"
#import "UINavigationController+Popover.h"
#import "MoneyStateEntry.h"

@interface IncomeExpenseBreakDownViewController()<BalanceSheetDataSourceDelegate>

@property(nonatomic, strong) BalanceSheetTabDataSource *balanceSheetDataSource;
@property(nonatomic, strong) IncomeExpenseBreakdownDataSource *dataSource;

@end

@implementation IncomeExpenseBreakDownViewController

+ (id)instanceWithDataSource:(BalanceSheetTabDataSource *)dataSource
{
    IncomeExpenseBreakDownViewController *viewController = [[IncomeExpenseBreakDownViewController alloc] initWithNibName:@"IncomeExpenseBreakDownViewController" bundle:nil];
    viewController.balanceSheetDataSource = dataSource;
    return viewController;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    id guide = self.topLayoutGuide;
    id bottomGuide = self.bottomLayoutGuide;
    id tableView = self.tableView;

    NSDictionary *views = NSDictionaryOfVariableBindings(guide,tableView,bottomGuide);
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[tableView]-0-[bottomGuide]" options:0 metrics:nil views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[tableView]|" options:0 metrics:nil views:views]];
    self.dataSource =  [IncomeExpenseBreakdownDataSource dataSourceWithDelegate:self andTableView:self.tableView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.dataSource viewWillAppear:animated];
    
}

#pragma mark - BalanceSheetDataSourceDelegate
- (id)selectedItem
{
    return [self.balanceSheetDataSource prepareEntryForEdit];
}

- (void)updateUI
{
    dispatch_async(dispatch_get_main_queue(), ^{
        //reload data
        NSLog(@"refresh IncomeExpenseBreakDownViewController");
        [self.tableView reloadData];
    });
    
}

- (void)reloadRowsAtIndexPaths:(NSArray<NSIndexPath *> *)indexPaths withRowAnimation:(UITableViewRowAnimation)animation
{
    [self.tableView beginUpdates];
    [self.tableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
    [self.tableView endUpdates];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AddNewEntrySelectionViewController *addNewEntrySelectionViewController = [story instantiateViewControllerWithIdentifier:@"AddNewEntrySelectionViewController"];
    addNewEntrySelectionViewController.dataSource = self.dataSource;
    MoneyStateEntry *selectedStateEntry = [self.dataSource selectedMoneyStateEntry];
    addNewEntrySelectionViewController.editMode = selectedStateEntry && selectedStateEntry.duplicateEntryExists;
    
    [UINavigationController presentPopoverWithRootViewController:addNewEntrySelectionViewController fromViewController:self withSourceView:[self.tableView cellForRowAtIndexPath:indexPath] withSize:CGSizeMake(400, 550)];
}

@end
