//
//  UIAlertController+Message.m
//  BalanceSheet
//
//  Created by Ye Ma on 2016-07-10.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "UIAlertController+Message.h"

@implementation UIAlertController (Message)

+(void)messageWithTitle:(NSString *)title andMessage:(NSString *)message fromViewController:(UIViewController *)viewController
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        //
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    [alertController addAction:cancelAction];
    [viewController presentViewController:alertController animated:YES completion:nil];
}

@end
