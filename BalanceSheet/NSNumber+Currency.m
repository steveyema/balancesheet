//
//  NSNumber+Currency.m
//  BalanceSheet
//
//  Created by Ye Ma on 2016-06-02.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "NSNumber+Currency.h"

@implementation NSNumber (Currency)

- (NSString *)standardFormat
{
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
    return [numberFormatter stringFromNumber:self];
}

+ (NSNumber *)unFormat:(NSString *)inputString
{
    
    NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    if ([inputString rangeOfCharacterFromSet:notDigits].location == NSNotFound)
    {
        return @([inputString doubleValue]);
    }
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
    return [numberFormatter numberFromString:inputString];
}

- (NSNumber *)archive
{
    return @(floor([self doubleValue] * 100));
}

- (NSNumber *)unarchive
{
    return @([self doubleValue] / 100);
}

- (NSNumber *)unarchivedPositive
{
    return @(([self doubleValue] < 0? -[self doubleValue] : [self doubleValue]) / 100);
}

@end
