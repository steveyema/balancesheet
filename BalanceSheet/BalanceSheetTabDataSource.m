//
//  BalanceSheetTabDataSource.m
//  BalanceSheet
//
//  Created by Ye Ma on 2016-06-12.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "BalanceSheetTabDataSource.h"

@interface BalanceSheetTabDataSource()

@property(nonatomic, assign) BOOL dataUpdated;

@end

@implementation BalanceSheetTabDataSource

- (id)init
{
    if (self = [super init])
    {
        _balanceSheetTab =  [BalanceSheetApplication sharedInstance].activeBalanceSheetTab;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(backContextDidSave:) name:NSManagedObjectContextDidSaveNotification object:[BalanceSheetDBController sharedInstance].backgroundManagedObjectContext];
        
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)backContextDidSave:(NSNotification *)notification
{
    NSSet *insertedSet = (NSSet *)notification.userInfo[@"inserted"];
    NSSet *updatedSet = (NSSet *)notification.userInfo[@"updated"];
    NSSet *deletedSet = (NSSet *)notification.userInfo[@"deleted"];
    
    if (insertedSet.count > 0 || updatedSet.count > 0 || deletedSet.count > 0)
    {
        self.dataUpdated = YES;
        [self onDataUpdate];
    }
}

- (void)onDataUpdate
{
    BalanceSheetTab activeTab = [[BalanceSheetApplication sharedInstance] activeBalanceSheetTab];
    
    if (activeTab == self.balanceSheetTab)
    {
        NSLog(@"refreshed %@ tab", self.viewControllerTypeString);
        [self updateDataScreen];
        self.dataUpdated = NO;
    }
}

- (BalanceSheetViewControllerType)viewControllerType
{
    if (self.balanceSheetTab == BalanceSheetTab_Income)
    {
        return BalanceSheetViewControllerType_Income;
    }
    else if (self.balanceSheetTab == BalanceSheetTab_Outcome)
    {
        return BalanceSheetViewControllerType_Outcome;
    }
    else
    {
        return BalanceSheetViewControllerType_Report;
    }
}

- (NSString *)viewControllerTypeString
{
    NSString *refreshedTab = @"Income";
    if (self.balanceSheetTab == BalanceSheetViewControllerType_Outcome)
    {
        refreshedTab = @"Expense";
    }
    else if (self.balanceSheetTab == BalanceSheetViewControllerType_Report)
    {
        refreshedTab = @"Report";
    }
    return refreshedTab;
}

- (void)viewWillAppear:(BOOL)animated
{

    if (self.dataUpdated)
    {
        [self updateDataScreen];
        self.dataUpdated = NO;
    }
}

@end
