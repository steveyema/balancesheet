//
//  ReportOperation.h
//  BalanceSheet
//
//  Created by Ye Ma on 2016-06-10.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, ReportOperationType) {
    ReportOperationType_Basic = 0,
    ReportOperationType_Detail,
    ReportOperationType_Breakdown
};

@class StateEntry;

@interface ReportOperation : NSOperation

@property(nonatomic, strong) StateEntry *stateEntry;

+ (id)operationWithType:(ReportOperationType)type andStateEntry:(StateEntry *)stateEntry;
- (NSArray *)stateEntriesBetweenStartDate:(NSDate *)startDate andEndDate:(NSDate *)endDate;
- (NSArray *)reoccurMoneyEntriesToInstances;

@end