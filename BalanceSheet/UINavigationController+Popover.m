//
//  UINavigationController+Popover.m
//  BalanceSheet
//
//  Created by Ye Ma on 2016-07-10.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "UINavigationController+Popover.h"

@implementation UINavigationController (Popover)

+(void)presentPopoverWithRootViewController:(UIViewController *)rootViewController fromViewController:(UIViewController *)viewController withSourceView:(UIView *)sourceView withSize:(CGSize)size
{
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:rootViewController];
    navigationController.modalPresentationStyle = UIModalPresentationPopover;
    rootViewController.preferredContentSize = size;
    
    navigationController.popoverPresentationController.sourceView = sourceView;
    navigationController.popoverPresentationController.sourceRect = CGRectMake(sourceView.bounds.size.width / 2.0, sourceView.bounds.size.height / 2.0, 1.0, 1.0);
    
    [viewController presentViewController:navigationController animated:YES completion:nil];
}

@end
