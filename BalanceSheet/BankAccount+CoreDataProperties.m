//
//  BankAccount+CoreDataProperties.m
//  BalanceSheet
//
//  Created by Ye Ma on 2016-06-02.
//  Copyright © 2016 Ye Ma. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "BankAccount+CoreDataProperties.h"

@implementation BankAccount (CoreDataProperties)

@dynamic name;
@dynamic balance;
@dynamic datetime;

@end
