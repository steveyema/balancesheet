//
//  UINavigationController+Popover.h
//  BalanceSheet
//
//  Created by Ye Ma on 2016-07-10.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationController (Popover)

+(void)presentPopoverWithRootViewController:(UIViewController *)rootViewController fromViewController:(UIViewController *)viewController withSourceView:(UIView *)sourceView withSize:(CGSize)size;
@end
