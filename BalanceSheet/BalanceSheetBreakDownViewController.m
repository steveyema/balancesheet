//
//  BalanceSheetBreakDownViewController.m
//  BalanceSheet
//
//  Created by Ye Ma on 2016-06-10.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "BalanceSheetBreakDownViewController.h"
#import "BalanceSheetDataSource.h"
#import "BalanceSheetBreakDownDataSource.h"

@interface BalanceSheetBreakDownViewController ()

@property (nonatomic, weak) id<BalanceSheetDataSourceDelegate> delegate;

@end

@implementation BalanceSheetBreakDownViewController

+ (id)viewControllerWithDelegate:(id<BalanceSheetDataSourceDelegate>)delegate
{
    BalanceSheetBreakDownViewController *viewController = [[BalanceSheetBreakDownViewController alloc] initWithNibName:@"BalanceSheetBreakDownViewController" bundle:nil];
    viewController.delegate = delegate;
    return viewController;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    id bottomGuide = self.bottomLayoutGuide;
    id tableView = self.tableView;
    
    NSDictionary *views = NSDictionaryOfVariableBindings(tableView,bottomGuide);
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[tableView][bottomGuide]|" options:0 metrics:nil views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[tableView]|" options:0 metrics:nil views:views]];

    self.dataSource = [BalanceSheetBreakDownDataSource dataSourceWithTableView:self.tableView andDelegate:self.delegate andCompletionBlock:^{
        [self.tableView reloadData];
    }];

    [self.dataSource viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
