//
//  BalanceSheetDataSource.m
//  BalanceSheet
//
//  Created by Ye Ma on 2016-05-31.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "IncomeExpenseDataSource.h"
#import "MoneyEntry+Reoccur.h"

@interface IncomeExpenseDataSource()<UITabBarDelegate, UITableViewDataSource>

@property(nonatomic, strong) NSMutableArray *listOfEntries;
@property(nonatomic, strong) MoneyEntry *selectedEntry;

- (void)fillAmountLabel:(UILabel *)label forIndexPath:(NSIndexPath *)indexPath;
- (NSNumber *)totalAmountForIndexPath:(NSIndexPath *)indexPath;
- (MoneyEntry *)entryForIndexPath:(NSIndexPath *)indexPath;
- (NSInteger)_tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
- (BOOL)_tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath;
- (void)_tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath;
- (void)_tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;

@end

@implementation IncomeExpenseDataSource

- (void)loadEntriesWithCompletionBlock:(void(^)(void))completionBlock
{
    [[BalanceSheetDBController sharedInstance] loadBankAccountFromBackgroundContextWithPredicate:nil withCompletionBlock:^(NSArray *results) {
        //
        NSArray *bankAccounts = [results mutableCopy];
        
        BankAccount *bankAccount = (bankAccounts && bankAccounts.count > 0)? bankAccounts.firstObject : nil;

        NSPredicate *predicate = [self predicateWithBalanceSheetViewControllerType:self.viewControllerType andBankAccount:bankAccount];
        [[BalanceSheetDBController sharedInstance] loadEntriesFromBackgroundContextWithPredicate:predicate withCompletionBlock:^(NSArray *results) {
            self.listOfEntries = [results mutableCopy];
            [self removeReoccuranceInstances];
            if (completionBlock)
            {
                completionBlock();
            }
        }];

    }];
}

- (NSPredicate *)predicateWithBalanceSheetViewControllerType:(BalanceSheetViewControllerType)viewControllerType andBankAccount:(BankAccount *)bankAccount
{
    NSPredicate *predicate = nil;
    NSString *amountFilter = nil;
    NSString *dateFilter = nil;
    
    switch (viewControllerType) {
        case BalanceSheetViewControllerType_Income:
        {
            amountFilter = @"amount >= 0";
            break;
        }
        case BalanceSheetViewControllerType_Outcome:
        {
            amountFilter = @"amount < 0";
            break;
        }
            
        default:
        {
            break;
        }
    }
    
    if (bankAccount)
    {
        dateFilter = @"(datetime > %@)";
    }
    
    NSMutableArray *filtersArray = [[NSMutableArray alloc] initWithCapacity:2];
    if (amountFilter) [filtersArray addObject:amountFilter];
    if (dateFilter) [filtersArray addObject:dateFilter];
    
    NSString *filter = [filtersArray componentsJoinedByString:@" AND "];
    
    if (dateFilter)
    {
        predicate = [NSPredicate predicateWithFormat:filter, bankAccount.datetime];
    }
    else
    {
        predicate = [NSPredicate predicateWithFormat:filter];
    }
    
    return predicate;
}

- (void)removeReoccuranceInstances
{
    NSMutableDictionary *hashTable = [[NSMutableDictionary alloc] initWithCapacity:self.listOfEntries.count];
    for (MoneyEntry *entry in self.listOfEntries)
    {
        if ([entry.type integerValue] != BalanceSheetEntryType_OneTime)
        {
            [hashTable setObject:entry forKey:entry.name];
        }
    }

    NSMutableArray *result = [[NSMutableArray alloc] initWithCapacity:self.listOfEntries.count];
    for (MoneyEntry *entry in self.listOfEntries)
    {
        if ([entry.type integerValue] != BalanceSheetEntryType_OneTime)
        {
            [result addObject:entry];
        }
        else
        {
            if ([hashTable objectForKey:entry.name])
            {
                continue;
            }
            else
            {
                [result addObject:entry];
            }
        }
    }
    
    self.listOfEntries = result;
}

#pragma mark - Abstract implementation
- (void)addNewEntry:(NSDictionary *)entryDic
{
    if (entryDic)
    {
        NSDate *date = [entryDic objectForKey:MONEYENTRY_DATETIME];
        
        MoneyEntry *entry = (MoneyEntry *)[NSEntityDescription insertNewObjectForEntityForName:MONEYENTRY
                                                                        inManagedObjectContext:[BalanceSheetDBController sharedInstance].mainManagedObjectContext];
        entry.name = [entryDic objectForKey:MONEYENTRY_NAME];
        entry.vdatetime = [date dateOnly];
        entry.datetime = [date dateOnly];
        entry.amount = [entryDic objectForKey:MONEYENTRY_AMOUNT];
        entry.type = [entryDic objectForKey:MONEYENTRY_TYPE];
        entry.disabled = [entryDic objectForKey:MONEYENTRY_DISABLED];
        
        NSError *error;
        if (![[BalanceSheetDBController sharedInstance].mainManagedObjectContext save:&error]) {
            NSLog(@"Failed to save - error: %@", [error localizedDescription]);
        }
    }
}

- (void)editExistingEntry:(NSDictionary *)entryDic
{
    if (entryDic)
    {
        NSDate *date = [entryDic objectForKey:MONEYENTRY_DATETIME];
        
        if (self.selectedEntry)
        {
            self.selectedEntry.name = [entryDic objectForKey:MONEYENTRY_NAME];
            self.selectedEntry.vdatetime = [date dateOnly];
            self.selectedEntry.datetime = [date dateOnly];
            self.selectedEntry.amount = [entryDic objectForKey:MONEYENTRY_AMOUNT];
            self.selectedEntry.type = [entryDic objectForKey:MONEYENTRY_TYPE];
            self.selectedEntry.disabled = [entryDic objectForKey:MONEYENTRY_DISABLED];
        }
        
        NSError *error;
        if (![[BalanceSheetDBController sharedInstance].mainManagedObjectContext save:&error]) {
            NSLog(@"Failed to save - error: %@", [error localizedDescription]);
        }
        
    }
}

- (void)fillAmountLabel:(UILabel *)label forIndexPath:(NSIndexPath *)indexPath
{
    MoneyEntry *moneyEntry = [self entryForIndexPath:indexPath];

    label.text = [NSString stringWithFormat:@"%@", [[moneyEntry.amount unarchivedPositive] standardFormat]];
}

- (NSInteger)_numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)_tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.listOfEntries.count > 0? self.listOfEntries.count + 1 : 0;
}

- (BOOL)_tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return indexPath.row < self.listOfEntries.count;
}

- (void)_tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //add code here for when you hit delete
        NSLog(@"deleting");
        [tableView beginUpdates];
        id objToRemove = self.listOfEntries[indexPath.row];
        if ([[BalanceSheetDBController sharedInstance] deleteManagedObject:objToRemove])
        {
            [self.listOfEntries removeObject:objToRemove];
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        }
        [tableView endUpdates];
    }
}

- (void)_tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < self.listOfEntries.count)
    {
        self.selectedEntry = self.listOfEntries[indexPath.row];
    }
}

- (UIView *)_tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}


- (NSNumber *)totalAmountForIndexPath:(NSIndexPath *)indexPath
{
    __block NSInteger total = 0;
    [self.listOfEntries enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        MoneyEntry *entry = (MoneyEntry *)obj;
        total += [entry.amount integerValue];
    }];
    return [@(total) unarchive];
}

- (MoneyEntry *)entryForIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < self.listOfEntries.count)
    {
        return (MoneyEntry *)self.listOfEntries[indexPath.row];
    }
    else
    {
        return nil;
    }
}

@end
