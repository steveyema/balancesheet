//
//  IncomeExpenseLoanViewController.h
//  BalanceSheet
//
//  Created by Ye Ma on 2016-07-15.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "BaseTableViewController.h"

@class IncomeExpenseLoanDataSource;

@interface IncomeExpenseLoanViewController : BaseTableViewController

@property (nonatomic, readonly, strong) IncomeExpenseLoanDataSource *dataSource;

@end
