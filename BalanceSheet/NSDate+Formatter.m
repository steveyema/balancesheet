//
//  NSDate+Formatter.m
//  BalanceSheet
//
//  Created by Ye Ma on 2016-06-02.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "NSDate+Formatter.h"

@implementation NSDate (Formatter)

- (NSString *)shortDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:1]];
    return [dateFormatter stringFromDate:self];
}

- (NSDate *)dateOnly
{
    
    unsigned int flags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
    NSCalendar* calendar = [NSCalendar currentCalendar];
    [calendar setTimeZone: [NSTimeZone timeZoneForSecondsFromGMT:1]];
    
    NSDateComponents* components = [calendar components:flags fromDate:self];
    
    NSDate *convertedDate = [calendar dateFromComponents:components];
    
    return convertedDate;
}

- (BOOL)isOlderThanDate:(NSDate *)date2
{
    //date1 is later than date2
    if ([[self dateOnly] compare:[date2 dateOnly]] == NSOrderedDescending)
    {
        return NO;
    }
    //date1 is earlier than date2
    else if ([[self dateOnly] compare:[date2 dateOnly]] == NSOrderedAscending)
    {
        return YES;
    }
    //dates are the same
    else
    {
        return NO;
    }
}

- (BOOL)isSameAsDate:(NSDate *)date2
{
    //date1 is later than date2
    if ([[self dateOnly] compare:[date2 dateOnly]] == NSOrderedDescending)
    {
        return NO;
    }
    //date1 is earlier than date2
    else if ([[self dateOnly] compare:[date2 dateOnly]] == NSOrderedAscending)
    {
        return NO;
    }
    //dates are the same
    else
    {
        return YES;
    }
}

- (NSDate *)adjustByNumberOfWeek:(NSInteger)numberOfWeek keepOriginalDate:(BOOL)keepOriginalDate
{
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setWeekdayOrdinal:numberOfWeek];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *newDate = [calendar dateByAddingComponents:dateComponents toDate:self options:0];
    
    if (!keepOriginalDate)
    {
        newDate = [newDate firstWeekDayForDate];
    }
    return newDate;
}

- (NSDate *)adjustByNumberOfMonth:(NSInteger)numberOfMonth keepOriginalDate:(BOOL)keepOriginalDate
{
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setMonth:numberOfMonth];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *newDate = [calendar dateByAddingComponents:dateComponents toDate:self options:0];
    
    if (!keepOriginalDate)
    {
        NSDateComponents *comp = [calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:newDate];
        [comp setDay:1];
        newDate = [calendar dateFromComponents:comp];
    }
    return newDate;
}

- (NSDate *)adjustByNumberOfYear:(NSInteger)numberOfYear keepOriginalDate:(BOOL)keepOriginalDate
{
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setYear:numberOfYear];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *newDate = [calendar dateByAddingComponents:dateComponents toDate:self options:0];

    if (!keepOriginalDate)
    {
        NSDateComponents *comp = [calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:newDate];
        [comp setDay:1];
        [comp setMonth:1];
        newDate = [calendar dateFromComponents:comp];
    }
    return newDate;
}

- (NSDate *)firstWeekDayForDate
{
    NSCalendar *cal = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    cal.firstWeekday = 2;// set first week day to Monday
    // 1: Sunday, 2: Monday, ..., 7:Saturday
    
    NSDate *startOfTheWeek;
    NSDate *endOfWeek;
    NSTimeInterval interval;
    [cal rangeOfUnit:NSCalendarUnitWeekOfYear
           startDate:&startOfTheWeek
            interval:&interval
             forDate:self];
    //startOfTheWeek holds the beginning of the week
    
    endOfWeek = [startOfTheWeek dateByAddingTimeInterval:interval - 1];
    // endOfWeek now holds the last second of the last week day
    
    [cal rangeOfUnit:NSCalendarUnitDay
           startDate:&endOfWeek
            interval:NULL
             forDate:endOfWeek];
    // endOfWeek now holds the beginning of the last week day
    return startOfTheWeek;
}

@end
