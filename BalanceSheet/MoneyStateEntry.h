//
//  MoneyStateEntry.h
//  BalanceSheet
//
//  Created by Ye Ma on 2016-07-03.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "StateEntry.h"

@interface MoneyStateEntry : StateEntry

@property(nonatomic, assign) BOOL duplicateEntryExists;
@property(nonatomic, strong) MoneyEntry *moneyEntry;

@end
