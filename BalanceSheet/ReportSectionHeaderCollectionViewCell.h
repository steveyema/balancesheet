//
//  ReportSectionHeaderCollectionViewCell.h
//  BalanceSheet
//
//  Created by Ye Ma on 2016-08-02.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReportSectionHeaderCollectionViewCell : UICollectionViewCell

@property(nonatomic, strong, readonly) UILabel *titleLabel;
+ (NSString *)reuseIdentifier;

@end
