//
//  MoneyEntry+Reoccur.h
//  BalanceSheet
//
//  Created by Ye Ma on 2016-06-03.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "MoneyEntry.h"

typedef NS_ENUM(NSInteger, BalanceSheetEntryType) {
    BalanceSheetEntryType_OneTime =  0,
    BalanceSheetEntryType_Reoccur,
    BalanceSheetEntryType_Reoccur_Week,
    BalanceSheetEntryType_Reoccur_Two_Weeks,
    BalanceSheetEntryType_Reoccur_Half_Month,
    BalanceSheetEntryType_Reoccur_Month,
    BalanceSheetEntryType_Reoccur_Two_Months,
    BalanceSheetEntryType_Reoccur_Count
};

@interface MoneyEntry (Reoccur)

- (NSNumber *)reoccurAmountFromStartDate:(NSDate *)startDate tillEndDate:(NSDate *)endDate;
- (NSArray *)reoccurInstanceFromStartDate:(NSDate *)startDate tillEndDate:(NSDate *)endDate;
- (NSDictionary *)reoccurInstanceFromIndex:(NSIndexPath *)indexPath1 toIndexPath:(NSIndexPath *)indexPath2 baseOn:(NSDate *)date;

@end
