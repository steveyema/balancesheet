//
//  IncomeOutcomeBaseTableViewController.m
//  BalanceSheet
//
//  Created by Ye Ma on 2016-06-02.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "IncomeExpenseViewController.h"
#import "AddNewEntryViewController.h"
#import "IncomeExpenseDataSource.h"
#import "AddNewEntrySelectionViewController.h"
#import "UIAlertController+Message.h"
#import "UINavigationController+Popover.h"

@interface IncomeExpenseViewController ()<BalanceSheetDataSourceDelegate>

@end

@implementation IncomeExpenseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    _dataSource = [IncomeExpenseBaseDataSource dataSourceFor:IncomeExpenseDataSourceType_Regular WithDelegate:self andTableView:self.tableView];
    
    self.title = self.dataSource.viewControllerTypeString;
    
    id guide = self.topLayoutGuide;
    id bottomGuide = self.bottomLayoutGuide;
    id tableView = self.tableView;
    
    NSDictionary *views = NSDictionaryOfVariableBindings(guide,tableView,bottomGuide);
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[guide]-0-[tableView]-0-[bottomGuide]" options:0 metrics:nil views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[tableView]|" options:0 metrics:nil views:views]];
    
    [self longPressGestureRecognizerSetup:tableView];

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [self.dataSource viewWillAppear:animated];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - BalanceSheetDataSourceDelegate

- (void)updateUI
{
    dispatch_async(dispatch_get_main_queue(), ^{
        //reload data
        NSLog(@"refresh IncomeExpenseViewController");
        [self.tableView reloadData];
    });
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    AddNewEntrySelectionViewController *addNewEntrySelectionViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"AddNewEntrySelectionViewController"];
    addNewEntrySelectionViewController.dataSource = self.dataSource;
    addNewEntrySelectionViewController.editMode = YES;
    
    UIView *view = [self.tableView cellForRowAtIndexPath:indexPath];
    [UINavigationController presentPopoverWithRootViewController:addNewEntrySelectionViewController fromViewController:self withSourceView:view withSize:CGSizeMake(400, 550)];
    
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    UIViewController *viewController = segue.destinationViewController;
    if ([viewController isKindOfClass:[UINavigationController class]])
    {
        viewController = ((UINavigationController *)viewController).topViewController;
    }
    if ([viewController isKindOfClass:[AddNewEntrySelectionViewController class]])
    {
        AddNewEntrySelectionViewController *addNewEntrySelectionViewController = (AddNewEntrySelectionViewController *)viewController;
        addNewEntrySelectionViewController.preferredContentSize = CGSizeMake(400, 550);
        addNewEntrySelectionViewController.dataSource = self.dataSource;
    }
}

- (IBAction)editTapped:(id)sender {
    [self.tableView setEditing:!self.tableView.editing];
}

#pragma mark - long press gesture
- (void)longPressGestureRecognizerSetup:(UITableView *)tableView
{
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.minimumPressDuration = 1.0;
    [tableView addGestureRecognizer:lpgr];
}

- (void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    CGPoint p = [gestureRecognizer locationInView:self.tableView];
    
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:p];
    if (!indexPath)
    {
        [NSException raise:@"Failed long press recongize" format:@"Fail to identify location in table view for long press"];
    }
    else if (gestureRecognizer.state == UIGestureRecognizerStateBegan)
    {
        NSLog(@"%ld", (long)indexPath.row);
        
        if (![self.dataSource presentBreakdownActionSheetForIndexPath:indexPath FromViewController:self withSourceView:[self.tableView cellForRowAtIndexPath:indexPath]])
        {
            [UIAlertController messageWithTitle:@"Invalid Action" andMessage:@"One time entry does not have breakdown" fromViewController:self];
        }
    }
}

@end
