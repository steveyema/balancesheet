//
//  ReportSectionHeaderDataSource.h
//  BalanceSheet
//
//  Created by Ye Ma on 2016-08-02.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ReportSectionHeaderDataSourceDelegate <NSObject>

- (void)reportSectionSelected:(NSIndexPath *)indexPath;

@end

@interface ReportSectionHeaderDataSource : NSObject

- (id)initWithCollectionView:(UICollectionView *)collectionView andDelegate:(id<ReportSectionHeaderDataSourceDelegate>)delegate;

@end
