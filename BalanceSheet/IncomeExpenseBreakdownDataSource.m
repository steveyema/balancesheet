//
//  IncomeExpenseBreakdownDataSource.m
//  BalanceSheet
//
//  Created by Ye Ma on 2016-07-03.
//  Copyright © 2016 Ye Ma. All rights reserved.
//

#import "IncomeExpenseBreakdownDataSource.h"
#import "ReportEntry.h"
#import "ReportSectionHeaderView.h"
#import "ReportEntryTableViewCell.h"
#import "MoneyEntryTableViewCell.h"
#import "MoneyEntry+Reoccur.h"
#import "MoneyStateEntry.h"

@interface IncomeExpenseBreakdownDataSource()<UITableViewDataSource, UITableViewDelegate>

@property(nonatomic, strong) NSIndexPath *selectedIndexPath;

@end

@implementation IncomeExpenseBreakdownDataSource

+(id)dataSourceWithDelegate:(id<BalanceSheetDataSourceDelegate>)delegate andTableView:(UITableView *)tableView
{
    IncomeExpenseBreakdownDataSource *dataSource = [[IncomeExpenseBreakdownDataSource alloc] initWithTableView:tableView];
    dataSource.delegate = delegate;
    return dataSource;
}

-(id)initWithTableView:(UITableView *)tableView
{
    if (self = [super init])
    {
        tableView.delegate = self;
        tableView.dataSource = self;
        
        [tableView registerNib:[UINib nibWithNibName:@"ReportEntryTableViewCell" bundle:nil] forCellReuseIdentifier:REPORT_ENTRY_CELL_IDENTIFIER];
        [tableView registerNib:[UINib nibWithNibName:@"ReportLoadingTableViewCell" bundle:nil] forCellReuseIdentifier:REPORT_LOADING_CELL_IDENTIFIER];
        
        [tableView registerClass:[ReportSectionHeaderView class] forHeaderFooterViewReuseIdentifier:[ReportSectionHeaderView uniqueIdentifier]];
        
        [self updateDataScreen];
    }
    return self;
}

- (MoneyStateEntry *)selectedMoneyStateEntry
{
    if (self.selectedIndexPath)
    {
        return self.reportEntries[self.selectedIndexPath];
    }
    else
    {
        return nil;
    }
}

#pragma mark - Abstract
- (ReportOperationType)reportOperationType
{
    return ReportOperationType_Breakdown;
}

- (void)updateDataScreen
{
    [self resetReportEntries];
    
    if ([self.delegate respondsToSelector:@selector(updateUI)])
    {
        [self.delegate updateUI];
    }
}

- (void)addNewEntry:(NSDictionary *)entryDic
{
    if (entryDic)
    {
        MoneyEntry *entry = (MoneyEntry *)[NSEntityDescription insertNewObjectForEntityForName:MONEYENTRY
                                                                        inManagedObjectContext:[BalanceSheetDBController sharedInstance].mainManagedObjectContext];
        NSDate *date = [entryDic objectForKey:MONEYENTRY_DATETIME];
        entry.name = [entryDic objectForKey:MONEYENTRY_NAME];
        entry.datetime = [date dateOnly];
        entry.amount = [entryDic objectForKey:MONEYENTRY_AMOUNT];
        entry.type = [entryDic objectForKey:MONEYENTRY_TYPE];
        entry.disabled = [entryDic objectForKey:MONEYENTRY_DISABLED];
        
        [self prepareDateFromMoneyEntry:[self prepareEntryForEdit] ForSaveMoneyEntry:entry];
        
        NSError *error;
        if (![[BalanceSheetDBController sharedInstance].mainManagedObjectContext save:&error]) {
            NSLog(@"Failed to save - error: %@", [error localizedDescription]);
        }
        
    }
    
}

- (void)prepareDateFromMoneyEntry:(MoneyEntry *)entryFrom ForSaveMoneyEntry:(MoneyEntry *)entryTo
{
    NSDate *dateTo = [entryTo.datetime dateOnly];
    NSDate *dateFrom = [entryFrom.datetime dateOnly];
    
    if (![dateFrom isEqual:dateTo])
    {
        entryTo.vdatetime = dateTo;
        entryTo.datetime = dateFrom;
    }
}

- (void)editExistingEntry:(NSDictionary *)entryDic
{
    if (entryDic)
    {
        NSDate *date = [entryDic objectForKey:MONEYENTRY_DATETIME];
        
        if (self.selectedIndexPath)
        {
            MoneyStateEntry *stateEntry = (MoneyStateEntry *)self.reportEntries[self.selectedIndexPath];
            
            stateEntry.moneyEntry.name = [entryDic objectForKey:MONEYENTRY_NAME];
            stateEntry.moneyEntry.vdatetime = [date dateOnly];
            stateEntry.moneyEntry.amount = [entryDic objectForKey:MONEYENTRY_AMOUNT];
            stateEntry.moneyEntry.type = [entryDic objectForKey:MONEYENTRY_TYPE];
            stateEntry.moneyEntry.disabled = [entryDic objectForKey:MONEYENTRY_DISABLED];
            
        }
        
        NSError *error;
        if (![[BalanceSheetDBController sharedInstance].mainManagedObjectContext save:&error]) {
            NSLog(@"Failed to save - error: %@", [error localizedDescription]);
        }
        
    }
}

- (void)preEntryCreation:(UIViewController *)viewController
{
//    NSString *typeString =
    
    MoneyStateEntry *stateEntry = (MoneyStateEntry *)self.reportEntries[self.selectedIndexPath];
    
    if (stateEntry.duplicateEntryExists)
    {
        [viewController setTitle:[NSString stringWithFormat:@"Edit %@", self.viewControllerTypeString]];
    }
    else
    {
        [viewController setTitle:[NSString stringWithFormat:@"Add %@", self.viewControllerTypeString]];
    }
}

- (id)prepareEntryForEdit
{
    return ((MoneyStateEntry *)self.reportEntries[self.selectedIndexPath]).moneyEntry;
}

- (void)postEntryForEdit
{
    self.selectedIndexPath = nil;
}

#pragma mark - UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 60;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    ReportSectionHeaderView *sectionHeader = (ReportSectionHeaderView *)[tableView dequeueReusableHeaderFooterViewWithIdentifier:[ReportSectionHeaderView uniqueIdentifier]];
    sectionHeader.headerLabel.text = @"Income/Expense Breakdown";
    return sectionHeader;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return PageSize * self.page + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == PageSize *self.page)
    {
        //end of the current list
        ReportEntryTableViewCell *cell = (ReportEntryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:REPORT_LOADING_CELL_IDENTIFIER];
        cell.completeBlock = ^{
            self.page ++;
            [tableView reloadData];
        };
        return cell;
    }
    else
    {

        ReportEntryTableViewCell *cell = (ReportEntryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:REPORT_ENTRY_CELL_IDENTIFIER];
        
        if (indexPath.row >= self.reportEntries.count)
        {
            MoneyEntry *entry = [self.delegate selectedItem];
            NSDictionary *reoccurInstances = [entry reoccurInstanceFromIndex:indexPath toIndexPath:[NSIndexPath indexPathForRow:indexPath.row + PageSize * self.page inSection:0] baseOn:entry.datetime];
            [self.reportEntries addEntriesFromDictionary:reoccurInstances];
        }
        MoneyStateEntry *moneyStateEntry = self.reportEntries[indexPath];
        
        
        //set amount;
        NSDate *date = moneyStateEntry.moneyEntry.vdatetime ? moneyStateEntry.moneyEntry.vdatetime : moneyStateEntry.moneyEntry.datetime;
        cell.date.text = [NSString stringWithFormat:@"%@", [date shortDate]];
        NSNumber *amount = [moneyStateEntry.moneyEntry.amount unarchive];
        
        NSString *formattedAmount = [amount standardFormat];
        cell.balance.text = [NSString stringWithFormat:@"%@", formattedAmount];
        
        if (moneyStateEntry.state == EntryState_New)
        {
            [self startOperationForReportEntry:moneyStateEntry atIndexPath:indexPath];
        }
        
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.delegate respondsToSelector:@selector(tableView:didSelectRowAtIndexPath:)])
    {
        self.selectedIndexPath = indexPath;
        [self.delegate tableView:tableView didSelectRowAtIndexPath:indexPath];
    }
}



@end
